<?php

namespace Eleadtech\Giftpoint\Model;

use Eleadtech\Giftpoint\Helper\Log;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Customer\Api\CustomerRepositoryInterface as CustomerRepository;

class Customer
{
    private $log = null;
    private $customerRepository = null;
    private $customerSession = null;
    private $addressMapper = null;
    private $addressConfig = null;
    private $countryFactory = null;
    
    public function __construct(
        Log $log,
        CustomerRepository $customerRepository,
        CustomerSession $customerSession,
        \Magento\Customer\Model\Address\Mapper $addressMapper,
        \Magento\Customer\Model\Address\Config $addressConfig,
        \Magento\Directory\Model\CountryFactory $countryFactory
    ) {
        
        $this->log = $log;
        $this->customerRepository = $customerRepository;
        $this->customerSession = $customerSession;
        $this->addressMapper = $addressMapper;
        $this->addressConfig = $addressConfig;
        $this->countryFactory = $countryFactory;
    }
    public function getCustomerData()
    {
        try {
            $customerData = [];
            
            if ($this->isCustomerLoggedIn()) {
                $customer = $this->customerRepository->getById($this->customerSession->getCustomerId());
                $customerData = $customer->__toArray();
                
                foreach ($customer->getAddresses() as $key => $address) {
                    $country = $this->countryFactory->create()->loadByCode($address->getCountryId());
                    $customerData['addresses'][$key]['inline'] = $this->getCustomerAddressInline($address);
                    $customerData['addresses'][$key]['country_name'] = $country->getName();
                }
            }
        } catch (\Exception $ex) {
            $this->log->writeMessage($ex->getMessage());
        }
        
        return $customerData;
    }
    
    private function getCustomerAddressInline($address)
    {
        $builtOutputAddressData = $this->addressMapper->toFlatArray($address);
        
        return $this->addressConfig->getFormatByCode(\Magento\Customer\Model\Address\Config::DEFAULT_ADDRESS_FORMAT)
            ->getRenderer()
            ->renderArray($builtOutputAddressData);
    }
    
    public function isCustomerLoggedIn()
    {
        if ($this->customerSession->isLoggedIn()) {
            return true;
        }
        return false;
    }
    
}
