<?php

namespace Eleadtech\Giftpoint\Model\Config\Source;

class Feetype
{
    /**#@+
     * Status values
     */
    const PERCENTAGE = 1;
    const FIXED = 2;
    /**
     * Retrieve option array
     *
     * @return string[]
     */
    public function toOptionArray()
    {
        return [
            ['label' => 'Percentage','value'=>self::PERCENTAGE],
            ['label' => 'Fixed','value'=>self::FIXED]
        ];
    }
    
    public static function getOptionArray()
    {
        return
            [
                self::PERCENTAGE => __('Percentage'),
                self::FIXED => __('Fixed'),   
            ];
    }

    /**
     * Retrieve option array with empty value
     *
     * @return string[]
     */
    public function getAllOptions()
    {
        $result = [];

        foreach (self::getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }

        return $result;
    }

    /**
     * Retrieve option text by option value
     *
     * @param string $optionId
     * @return string
     */
    public static function getOptionText($optionId)
    {
        $options = self::getOptionArray();

        return isset($options[$optionId]) ? $options[$optionId] : null;
    }
   
}
