<?php

namespace Eleadtech\Giftpoint\Model;

class Status
{
    /**#@+
     * Status values
     */
  
    /**
     * Retrieve option array
     *
     * @return string[]
     */
    public static function getOptionArray()
    {
        return
            [
                \Eleadtech\Giftpoint\Model\Egiftpointprocess::PENDING => __('PENDING'),
                \Eleadtech\Giftpoint\Model\Egiftpointprocess::COMPLETED => __('PROCESSED'),
                \Eleadtech\Giftpoint\Model\Egiftpointprocess::REFUND => __('REFUND'),
                \Eleadtech\Giftpoint\Model\Egiftpointprocess::EXCHANGED => __('EXCHANGED'),
                
            ];
    }

    /**
     * Retrieve option array with empty value
     *
     * @return string[]
     */
    public function getAllOptions()
    {
        $result = [];

        foreach (self::getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }

        return $result;
    }

    /**
     * Retrieve option text by option value
     *
     * @param string $optionId
     * @return string
     */
    public function getOptionText($optionId)
    {
        $options = self::getOptionArray();

        return isset($options[$optionId]) ? $options[$optionId] : null;
    }
    
    public static function getStatusLabelById($statusId)
    {
        $statuses = self::getOptionArray();
        if (isset($statuses[$statusId])) {
            return $statuses[$statusId];
        }
        return '';
    }
}
