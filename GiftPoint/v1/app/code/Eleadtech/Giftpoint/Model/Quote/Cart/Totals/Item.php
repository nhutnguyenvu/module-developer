<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eleadtech\Giftpoint\Model\Quote\Cart\Totals;

/**
 * Cart item totals.
 *
 * @codeCoverageIgnore
 */
class Item extends \Magento\Quote\Model\Cart\Totals\Item
{
    const KEY_GIFT_POINT = 'gift_point';
    
    public function getGiftPoint()
    {
        return $this->_get(self::KEY_GIFT_POINT);
    }
    
    public function setGiftPoint($giftPoint)
    {
        return $this->setData(self::KEY_GIFT_POINT, $giftPoint);
    }
}
