<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace  Eleadtech\Giftpoint\Model\Sales\Order;

class Item extends \Magento\Sales\Model\Order\Item
{
    public function getDescription()
    {
        
        $point = $this->getGiftPoint();
        if ($point > 0) {
            return parent::getDescription() . __('Gift Point: %1', $point);
        }
        return parent::getDescription();
    }
}
