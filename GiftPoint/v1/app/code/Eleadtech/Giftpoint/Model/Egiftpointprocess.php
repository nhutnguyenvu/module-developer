<?php
namespace Eleadtech\Giftpoint\Model;

class Egiftpointprocess extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    const PENDING = 1;
    const COMPLETED = 2;
    const REFUND = 3;
    const EXCHANGED = 4;

    protected function _construct()
    {
        $this->_init('Eleadtech\Giftpoint\Model\ResourceModel\Egiftpointprocess');
    }
}
