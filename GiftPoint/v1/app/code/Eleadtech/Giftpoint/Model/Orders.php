<?php
namespace Eleadtech\Giftpoint\Model;

use Eleadtech\Giftpoint\Helper\Data;
use Eleadtech\Giftpoint\Helper\Log;

class Orders
{
    
    protected $helper = null;
    protected $log = null;
    protected $customerRepository = null;
    protected $orderService = null;
    protected $cartRepositoryInterface = null;
    protected $cartManagementInterface = null;
    protected $shippingRate = null;
    
    public function __construct(
        \Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface,
        \Magento\Quote\Api\CartManagementInterface $cartManagementInterface,
        \Magento\Quote\Model\Quote\Address\Rate $shippingRate,
        Data $helper,
        Log $log
    ) {
        $this->cartRepositoryInterface = $cartRepositoryInterface;
        $this->cartManagementInterface = $cartManagementInterface;
        $this->shippingRate = $shippingRate;
        $this->helper = $helper;
        $this->log = $log;
    }

    /**
     * Create Order On Your Store
     *
     * @param array $orderData
     * @return int $orderId
     *
     */
    public function createOrder($skus, $addressId)
    {
        
        
        try {
            if (empty($skus) || empty($addressId)) {
                return false;
            }
            //init the store id and website id @todo pass from array
            $store = $this->helper->getStoreManager()->getStore();
            //$websiteId = $this->getStoreManager()->getStore()->getWebsiteId();

            //init the quote
            $cartId = $this->cartManagementInterface->createEmptyCart();
            $cart = $this->cartRepositoryInterface->get($cartId);
            $cart->setStore($store);
            // if you have already buyer id then you can load customer directly
            $customer = $this->helper->getCustomerRepository()->getById($this->helper->getCustomerSession()->getCustomer()->getId());
            $cart->setCurrency();
            $cart->assignCustomer($customer); //Assign quote to customer
            //add items in quote
            $cart->setIsGiftRequest(1);
            $totalSpendPoint = $this->_validateAvailablePoint($skus);
            if (!$totalSpendPoint) {
                return false;
            }
            foreach ($skus as $sku) {
                $product = $this->helper->getProductFactory()->create()->loadByAttribute("sku", $sku);
                $product->setPrice(0);
                $product->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
                $cart->addProduct(
                    $product,
                    intval(1)
                );
            }
            $address = $this->_getAddress($addressId);
            if (!$address) {
                $this->log(__("Invalid Address for Customer: %1", $customer->getEmail()));
                return false;
            }
            
            $cart->getBillingAddress()->addData($address);
            $cart->getShippingAddress()->addData($address);

            // Collect Rates and Set Shipping & Payment Method
            $this->shippingRate
                ->setCode('giftshipping_giftshipping')
                ->getPrice(true);

            $shippingAddress = $cart->getShippingAddress();

            //@todo set in order data
            $shippingAddress->setCollectShippingRates(true)
                ->collectShippingRates()
                ->setShippingMethod('giftshipping_giftshipping'); //shipping method

            $cart->getShippingAddress()->addShippingRate($this->shippingRate);
            $cart->setPaymentMethod('free'); //payment method

            //@todo insert a variable to affect the invetory
            $cart->setInventoryProcessed(false);

            // Set sales order payment
            $cart->getPayment()->importData(['method' => 'free']);

            // Collect total and saeve
           
            $cart->setTotalExchangedPoint($totalSpendPoint);
            $cart->collectTotals();

            // Submit the quote and create the order
            $cart->save();

            $cart = $this->cartRepositoryInterface->get($cart->getId());

            $orderId = $this->cartManagementInterface->placeOrder($cart->getId());
            
            $this->_updateCustomerPoint($totalSpendPoint);
            
            return $orderId;
        } catch (\Exception $ex) {
            $this->log->writeLog($ex->getMessage());
            return false;
        }
    }
    
    protected function _getAddress($addressId)
    {
        
        $customer = $this->helper->getCustomerSession()->getCustomer();
        $address = $customer->getAddressById($addressId);
        if ($address) {
            return $address->getData();
        }
        return false;
    }
    
    protected function _updateCustomerPoint($totalSpendPoint)
    {
        
        $customer = $this->helper->getCustomerRepository()->getById($this->helper->getCustomerSession()->getCustomer()->getId());
        $object = $customer->getCustomAttribute('gift_point');
        $point = 0;
        if ($object) {
            $point = $object->getValue();
        }
        $newPoint = intval($point) -  intval($totalSpendPoint);
        $customer->setCustomAttribute('gift_point', $newPoint);
        $this->helper->getCustomerRepository()->save($customer);
    }
    
    protected function _validateAvailablePoint($skus)
    {
        $totalPoint = 0;
        if (!empty($skus)) {
            $totalPoint = $this->helper->getTotalGiftPointBySku($skus);
            if ($totalPoint==0) {
                $customer = $this->helper->getCustomerSession()->getCustomer();
                $this->log->writeLog(__("Some products are not exchanged gift: %1", $customer->getEmail()));
                return false;
            }
        }
        if ($this->helper->getCustomerAvailablePoint() < $totalPoint) {
            $customer = $this->helper->getCustomerSession()->getCustomer();
            $this->log->writeLog(__("Available Point is lower Request Exchange Point: %1", $customer->getEmail()));
            return false;
        }
        
        return $totalPoint;
    }
}
