<?php

namespace Eleadtech\Giftpoint\Model;

use Eleadtech\Giftpoint\Helper\Data;
use Eleadtech\Giftpoint\Helper\Log;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\App\ObjectManager;

class EgiftpointprocessFactory
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;
    private $helper;
    private $log;
    private $date;
    

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        Data $helper,
        Log $log,
        DateTime $date
    ) {
        $this->objectManager = $objectManager;
        $this->helper = $helper;
        $this->log = $log;
        $this->date = $date;
    }

    /**
     * Create new country model
     *
     * @param array $arguments
     * @return \Magento\Directory\Model\Country
     */
    public function create(array $arguments = [])
    {
        return $this->objectManager->create('Eleadtech\Giftpoint\Model\Egiftpointprocess', $arguments, false);
    }
    
    public function createGiftPointByOrder($order)
    {
        
        try {
            if ($order->getCustomerId() && $order->getTotalGiftPoint() > 0) {
                $date =  $this->date->gmtDate();
                $data = ["customer_id"=>$order->getCustomerId(),
                        "point"=>intval($order->getTotalGiftPoint()),
                        "status"=>\Eleadtech\Giftpoint\Model\Egiftpointprocess::PENDING,
                        "increment_id"=>$order->getIncrementId(),
                        "created_at"=>$date,
                        "updated_at"=>$date,
                        "comment"=>"",
                    ];
                
                $this->create()->setData($data)->save();
                return $data;
            }
        } catch (\Exception $ex) {
            $this->log->writeLog($ex->getMessage());
            return false;
        }
        return false;
    }
    
    public function updateStatusByIncrementId($orderIncrementId, $status, $order = false)
    {
        
        
        try {
            $allow = $this->_allowGiftPointUpdate($orderIncrementId);
            if ($allow) {
                if (!$order) {
                    $order = $this->objectManager->get("\Magento\Sales\Api\Data\OrderInterface");
                    $order->loadByIncrementId($orderIncrementId);
                }
                $condition = [' increment_id = (?)' => $orderIncrementId ];
                $result = $this->create()->getResource()->updateStatusByCondition($condition, $status, $this->helper);
                //make point to $cusomer
                
                if ($result) {
                    $customer = $this->helper->getCustomerRepository()->getById($order->getCustomerId());
                    $object = $customer->getCustomAttribute('gift_point');
                    $point = 0;
                    if ($object) {
                        $point = $object->getValue();
                    }
                    $newPoint = intval($point) + intval($order->getTotalGiftPoint());
                    $customer->setCustomAttribute('gift_point', $newPoint);
                    $this->helper->getCustomerRepository()->save($customer);
                }
            }
        } catch (\Exception $ex) {
            $this->log->writeLog($ex->getMessage());
        }
        return false;
    }
    
    protected function _allowGiftPointUpdate($orderIncrementId)
    {
        
        $collection = $this->create()->getCollection()
            ->addFieldToFilter("increment_id", $orderIncrementId)
            ->addFieldToFilter("status", \Eleadtech\Giftpoint\Model\Egiftpointprocess::COMPLETED);
        
        if (!$collection) {
            return true;
        } elseif (!$collection->count()) {
            return true;
        }
        return false;
    }
    public function getGiftData($customerId = false, $page = 1)
    {
        
        if (empty($customerId)) {
            $customerId = $this->helper->getCustomerSession()->getCustomer()->getId();
        }
        
        $collection = $this->create()->getCollection()
                ->filterByCustomerId($customerId)
                ->setOrder('increment_id', 'DESC');
        
        //$list = $this->_helper->makeGiftCollectionToArray($collection);
        $collection->setPageSize(10);
        $collection->setCurPage($page);
        return $collection;
    }
   
    public function getCustomerAvailablePoint()
    {
        
        return $this->helper->getCustomerAvailablePoint();
    }
    
   
}
