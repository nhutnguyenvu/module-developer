<?php
namespace Eleadtech\Giftpoint\Model;

use Magento\Framework\Mail\Template\TransportBuilder;
use Eleadtech\Giftpoint\Helper\Data;
use Eleadtech\Giftpoint\Helper\Log;
use Magento\Framework\Stdlib\DateTime\DateTime;

class FriendpointFactory
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;
    private $helper;
    private $log;
    private $date;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        Data $helper,
        Log $log,
        DateTime $date,
        TransportBuilder $transportBuilder
    )
    {
        $this->_objectManager = $objectManager;
        $this->helper = $helper;
        $this->log = $log;
        $this->date = $date;
        $this->transportBuilder = $transportBuilder;
        
    }

    /**
     * Create new country model
     *
     * @param array $arguments
     * @return \Magento\Directory\Model\Country
     */
    public function create(array $arguments = [])
    {
        return $this->_objectManager->create('Eleadtech\Giftpoint\Model\Friendpoint', $arguments, false);
    }
    
    public function sendPointToFriend($email,$sendingPoint,$message){
        
        $point = $this->helper->getCustomerAvailablePoint();
        $sendingPoint = intval($sendingPoint);
        if($sendingPoint > $point){
            throw new \Exception('Invalid Point Amount');
        }
        $receiverCustomerId = $this->helper->getCustomerIdByEmail(trim($email));
        if(empty($receiverCustomerId)){
            throw new \Exception(__("Customer not exist"));
        }
        
        $message = strip_tags($message);
        return $this->_createRecord($receiverCustomerId,$sendingPoint,$message);
    }
    
    protected function _sendEmail($receiverCustomerId,$receivePoint,$message){
        
        $receiver = $this->helper->getEmailObjectByCustomerId($receiverCustomerId);
        $sender = $this->helper->getEmailObjectByCustomerId($this->helper->getCurrentCustomerId());
        $salesSender = $this->helper->getSaleSender();
        $emailTempVariables = array();
        
        $emailTempVariables['message'] = $message;
        $emailTempVariables['receiver_point'] = $receivePoint;
        $emailTempVariables['receiver_name'] = $receiver['name'];
        $emailTempVariables['sender_info'] =  $sender['name'] . " (" . $sender['email'] . ")";
        
        $postObject = new \Magento\Framework\DataObject();
        $postObject->setData($emailTempVariables);

        
        $transport = $this->transportBuilder->setTemplateIdentifier('sending_point_email_template')
        ->setTemplateOptions(['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID])
        ->setTemplateVars(['data' => $postObject])
        ->setFrom($salesSender)
        ->addTo($receiver['email'])
        ->setReplyTo($sender['email'])            
        ->getTransport();   
        
        $transport->sendMessage();
        return $this;
    }
    
    protected function _reducePoint($reducePoint,$customerId){
        try{
            $reducePoint = abs(intval($reducePoint));
            $customer = $this->helper->getCustomerRepository()->getById($customerId);
            if($customer){
                $point = $this->helper->getCustomerAvailablePoint($customerId);
                $newPoint = intval($point) - intval($reducePoint);
                if($newPoint <= 0){
                    $newPoint  = 0;
                }
                $customer->setCustomAttribute('gift_point', $newPoint);
                $this->helper->getCustomerRepository()->save($customer);
            }
        } catch (\Exception $ex) {
            $this->log->writeLog($ex->getMessage());
            return false;
        }
        return true;
    }
    
    protected function _increasePoint($increasePoint,$customerId){
        try{
            $increasePoint = abs(intval($increasePoint));
            $customer = $this->helper->getCustomerRepository()->getById($customerId);
            if($customer){
                $point = $this->helper->getCustomerAvailablePoint($customerId);
                $newPoint = intval($point) + intval($increasePoint);
                $customer->setCustomAttribute('gift_point', $newPoint);
                $this->helper->getCustomerRepository()->save($customer);
            }
        } catch (\Exception $ex) {
            $this->log->writeLog($ex->getMessage());
            return false;
        }
        return true;
    }
    protected function _createRecord($receiverCustomerId,$sendingPoint,$message,$sendMail = true){
        
        try {
            $fee = $this->helper->calculateTrasferFee($sendingPoint);
            $data = ["sender_customer_id"=>$this->helper->getCurrentCustomerId(),
                    "receiver_customer_id"=>$receiverCustomerId,
                    "website_id"=>$this->helper->getStoreManager()->getWebsite()->getWebsiteId(),
                    "point_transfer"=>$sendingPoint,
                    "created_at"=>$this->date->gmtDate(),
                    "fee"=>$fee,
                    "comment"=>$message
                ];
            
            $this->create()->setData($data)->save();
            $this->_reducePoint($sendingPoint,$this->helper->getCurrentCustomerId());
            $this->_increasePoint($sendingPoint  - $fee,$receiverCustomerId);
            
            try{
                if($sendMail){
                    $this->_sendEmail($receiverCustomerId, $sendingPoint  - $fee, $message);
                }
            } catch (\Exception $ex) {
                $this->log->writeLog($ex->getMessage());
            }
            
            return $data;
            
        } catch (\Exception $ex) {
            $this->log->writeLog($ex->getMessage());
            throw new \Exception(__('Some Errors happened, we are really sorry about this'));
            //return false;
        }
        return false;
    }
}