<?php
namespace Eleadtech\Giftpoint\Model;

class Friendpoint extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Eleadtech\Giftpoint\Model\ResourceModel\Friendpoint');
    }
}

