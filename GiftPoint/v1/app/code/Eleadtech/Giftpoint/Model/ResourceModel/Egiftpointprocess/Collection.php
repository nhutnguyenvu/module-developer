<?php

namespace Eleadtech\Giftpoint\Model\ResourceModel\Egiftpointprocess;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Eleadtech\Giftpoint\Model\Egiftpointprocess', 'Eleadtech\Giftpoint\Model\ResourceModel\Egiftpointprocess');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }
    
    public function filterByCustomerId($customerId)
    {
        return $this->addFieldToFilter("customer_id", $customerId);
    }
}
