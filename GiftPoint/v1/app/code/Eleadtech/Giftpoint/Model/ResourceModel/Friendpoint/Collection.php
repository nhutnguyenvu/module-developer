<?php

namespace Eleadtech\Giftpoint\Model\ResourceModel\Friendpoint;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Eleadtech\Giftpoint\Model\Friendpoint', 'Eleadtech\Giftpoint\Model\ResourceModel\Friendpoint');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}

