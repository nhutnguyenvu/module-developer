<?php
namespace Eleadtech\Giftpoint\Model\ResourceModel;

class Egiftpointprocess extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('egift_point_process', 'id');
    }
    
    public function updateStatusByCondition($condition, $status, $log = false)
    {
        
        try {
            $connection = $this->getConnection();
            $connection->update($this->getMainTable(), ['status' => $status], $condition);
        } catch (\Exception $ex) {
            if ($log) {
                $log->writeLog($ex->getMessage());
            }
            return false;
        }
        return true;
    }
    
    public function getGiftData()
    {
    }
}
