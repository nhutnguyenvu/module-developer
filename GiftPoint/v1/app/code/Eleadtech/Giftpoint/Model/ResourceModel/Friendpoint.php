<?php
namespace Eleadtech\Giftpoint\Model\ResourceModel;

class Friendpoint extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('egift_point_friend_process', 'id');
    }
}

