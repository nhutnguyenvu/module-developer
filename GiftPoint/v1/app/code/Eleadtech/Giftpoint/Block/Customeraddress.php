<?php
namespace Eleadtech\Giftpoint\Block;

use Eleadtech\Giftpoint\Helper\Data;
use Eleadtech\Giftpoint\Model\Customer;
use Magento\Framework\View\Element\Template;

class Customeraddress extends Template
{
    private $helper;
    private $customer;
    
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        Data $helper,
        Customer $customer,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->helper = $helper;
        $this->customer = $customer;
    }
    
    public function getCustomerData()
    {
        return $this->customer->getCustomerData();
    }
    
    public function getNewAddressUrl()
    {
        return $this->helper->getNewAddressUrl();
    }
}
