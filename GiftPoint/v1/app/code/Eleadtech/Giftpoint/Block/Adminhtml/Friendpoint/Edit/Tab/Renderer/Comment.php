<?php
namespace Eleadtech\Giftpoint\Block\Adminhtml\Friendpoint\Edit\Tab\Renderer;

use Magento\Framework\DataObject;

class Comment extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    protected $filter;
    public function __construct(\Magento\Framework\Filter\FilterManager $filter)
    {
        $this->filter = $filter;
    }
    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    
    /**
     * get category name
     * @param  DataObject $row
     * @return string
     */
    public function render(DataObject $row)
    {
        $comment = $row->getComment();
        if(strlen($comment) < 50){
            return $comment;
        }
        $string = $this->filter->truncate($comment, ['length' => 50]);
        return "<span title='$comment'>".$string . "... hover to see more</span>";
    }
}
