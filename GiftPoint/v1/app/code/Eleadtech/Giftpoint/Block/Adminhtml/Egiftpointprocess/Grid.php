<?php
namespace Eleadtech\Giftpoint\Block\Adminhtml\Egiftpointprocess;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Eleadtech\Giftpoint\Model\egiftpointprocessFactory
     */
    protected $_egiftpointprocessFactory;

    /**
     * @var \Eleadtech\Giftpoint\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Eleadtech\Giftpoint\Model\egiftpointprocessFactory $egiftpointprocessFactory
     * @param \Eleadtech\Giftpoint\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Eleadtech\Giftpoint\Model\EgiftpointprocessFactory $EgiftpointprocessFactory,
        \Eleadtech\Giftpoint\Model\Status $status,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {
        $this->_egiftpointprocessFactory = $EgiftpointprocessFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_egiftpointprocessFactory->create()->getCollection();
        $joinConditions = 'main_table.customer_id = customer_entity.entity_id';
        $collection->getSelect()->join(
            ['customer_entity'],
            $joinConditions,
            ['email' => 'customer_entity.email']
        );
        
        $this->setCollection($collection);
        

        parent::_prepareCollection();

        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );

        $this->addColumn(
            'email',
            [
                    'header' => __('Email'),
                    'index' => 'email',
                ]
        );
    /*
        $this->addColumn(
                'customer_id',
                [
                    'header' => __('Customer Id'),
                    'index' => 'customer_id',
                ]
        );
        */
        $this->addColumn(
            'increment_id',
            [
                        'header' => __('Increment Id'),
                        'index' => 'increment_id',
                ]
        );

        $this->addColumn(
            'point',
            [
                    'header' => __('Point'),
                    'index' => 'point',
                ]
        );
        
        $this->addColumn(
            'status',
            [
                        'header' => __('Status'),
                        'index' => 'status',
                        'type' => 'options',
                        'options' => $this->_status->getOptionArray()
                ]
        );
        
        $this->addColumn(
            'updated_at',
            [
                    'header' => __('Create At'),
                    'index' => 'updated_at',
                    'type'      => 'datetime',
                ]
        );
                    
                    


        
        //$this->addColumn(
            //'edit',
            //[
                //'header' => __('Edit'),
                //'type' => 'action',
                //'getter' => 'getId',
                //'actions' => [
                    //[
                        //'caption' => __('Edit'),
                        //'url' => [
                            //'base' => '*/*/edit'
                        //],
                        //'field' => 'id'
                    //]
                //],
                //'filter' => false,
                //'sortable' => false,
                //'index' => 'stores',
                //'header_css_class' => 'col-action',
                //'column_css_class' => 'col-action'
            //]
        //);
        

        
        $this->addExportType($this->getUrl('giftpoint/*/exportCsv', ['_current' => true]), __('CSV'));
        $this->addExportType($this->getUrl('giftpoint/*/exportExcel', ['_current' => true]), __('Excel XML'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

    
    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('id');
        //$this->getMassactionBlock()->setTemplate('Eleadtech_Giftpoint::egiftpointprocess/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('egiftpointprocess');

        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => __('Delete'),
                'url' => $this->getUrl('giftpoint/*/massDelete'),
                'confirm' => __('Are you sure?')
            ]
        );

        $statuses = $this->_status->getOptionArray();

        $this->getMassactionBlock()->addItem(
            'status',
            [
                'label' => __('Change status'),
                'url' => $this->getUrl('giftpoint/*/massStatus', ['_current' => true]),
                'additional' => [
                    'visibility' => [
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('Status'),
                        'values' => $statuses
                    ]
                ]
            ]
        );


        return $this;
    }
        

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('giftpoint/*/index', ['_current' => true]);
    }

    /**
     * @param \Eleadtech\Giftpoint\Model\egiftpointprocess|\Magento\Framework\Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
        
        return $this->getUrl(
            'giftpoint/*/edit',
            ['id' => $row->getId()]
        );
    }
}
