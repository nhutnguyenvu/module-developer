<?php
namespace Eleadtech\Giftpoint\Block\Adminhtml\Friendpoint;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{

    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Eleadtech\Giftpoint\Model\friendpointFactory
     */
    protected $_friendpointFactory;

    /**
     * @var \Eleadtech\Giftpoint\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Eleadtech\Giftpoint\Model\friendpointFactory $friendpointFactory
     * @param \Eleadtech\Giftpoint\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
    \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Eleadtech\Giftpoint\Model\FriendpointFactory $FriendpointFactory,
        \Eleadtech\Giftpoint\Model\Status $status,
        \Magento\Framework\Module\Manager $moduleManager, array $data = []
    )
    {
        $this->_friendpointFactory = $FriendpointFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_friendpointFactory->create()->getCollection();
        $this->setCollection($collection);
        
        $joinConditions = 'main_table.website_id = store_website.website_id';
        
        $collection->getSelect()->join(
            ['store_website'],
            $joinConditions,
            ['website_name' => 'store_website.name']
        );
        $this->setCollection($collection);
         
        parent::_prepareCollection();

        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {

        $this->addColumn(
            'sender',
            [
                'header' => __('Sender'),
                'sortable' => true,
                'index' => 'sender',
                'renderer' => 'Eleadtech\Giftpoint\Block\Adminhtml\Friendpoint\Edit\Tab\Renderer\Sender'
            ]
        );
        $this->addColumn(
            'receiver',
            [
                'header' => __('Receiver'),
                'sortable' => true,
                'index' => 'receiver',
                'renderer' => 'Eleadtech\Giftpoint\Block\Adminhtml\Friendpoint\Edit\Tab\Renderer\Receiver'
            ]
        );

        $this->addColumn(
            'website_name', [
            'header' => __('Website'),
            'index' => 'website_name',
            ]
        );

        $this->addColumn(
            'point_transfer', [
            'header' => __('Point Transfer'),
            'index' => 'point_transfer',
            ]
        );
        $this->addColumn(
            'comment',
            [
                'header' => __('Comment'),
                'sortable' => true,
                'index' => 'comment',
                'renderer' => 'Eleadtech\Giftpoint\Block\Adminhtml\Friendpoint\Edit\Tab\Renderer\Comment'
            ]
        );
        $this->addColumn(
            'fee', [
            'header' => __('Fee'),
            'index' => 'fee',
            ]
        );
        

        $this->addColumn(
            'created_at', [
            'header' => __('Created At'),
            'index' => 'created_at',
            'type' => 'datetime',
            ]
        );





        //$this->addColumn(
        //'edit',
        //[
        //'header' => __('Edit'),
        //'type' => 'action',
        //'getter' => 'getId',
        //'actions' => [
        //[
        //'caption' => __('Edit'),
        //'url' => [
        //'base' => '*/*/edit'
        //],
        //'field' => 'id'
        //]
        //],
        //'filter' => false,
        //'sortable' => false,
        //'index' => 'stores',
        //'header_css_class' => 'col-action',
        //'column_css_class' => 'col-action'
        //]
        //);



        $this->addExportType($this->getUrl('giftpoint/*/exportCsv', ['_current' => true]), __('CSV'));
        $this->addExportType($this->getUrl('giftpoint/*/exportExcel', ['_current' => true]), __('Excel XML'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('id');
        //$this->getMassactionBlock()->setTemplate('Eleadtech_Giftpoint::friendpoint/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('friendpoint');

        $this->getMassactionBlock()->addItem(
            'delete', [
            'label' => __('Delete'),
            'url' => $this->getUrl('giftpoint/*/massDelete'),
            'confirm' => __('Are you sure?')
            ]
        );

        $statuses = $this->_status->getOptionArray();

        $this->getMassactionBlock()->addItem(
            'status', [
            'label' => __('Change status'),
            'url' => $this->getUrl('giftpoint/*/massStatus', ['_current' => true]),
            'additional' => [
                'visibility' => [
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => __('Status'),
                    'values' => $statuses
                ]
            ]
            ]
        );


        return $this;
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {

        return $this->getUrl('giftpoint/*/index', ['_current' => true]);
    }

    /**
     * @param \Eleadtech\Giftpoint\Model\friendpoint|\Magento\Framework\Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return false;
    }
}
