<?php
namespace Eleadtech\Giftpoint\Block\Adminhtml\Friendpoint\Edit\Tab\Renderer;

use Magento\Framework\DataObject;

class Sender extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    private $helper;

    /**
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     */
    public function __construct(\Eleadtech\GiftPoint\Helper\Data $helper)
    {
        $this->helper = $helper;
    }

    /**
     * get category name
     * @param  DataObject $row
     * @return string
     */
    public function render(DataObject $row)
    {
       $customer = $this->helper->getCustomerRepository()->getById($row->getSenderCustomerId());
       if($customer){
           return $customer->getEmail();
       }
       return "";
    }
}
