<?php
namespace Eleadtech\Giftpoint\Block\Adminhtml\Friendpoint\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('friendpoint_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Friendpoint Information'));
    }
}