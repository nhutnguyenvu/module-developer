<?php
namespace Eleadtech\Giftpoint\Block;

use Eleadtech\Giftpoint\Helper\Data;
use Eleadtech\Giftpoint\Helper\Config;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\Template;
use Magento\Framework\App\RequestInterface;

class Giftpoint extends Template
{
    protected $helper;
    protected $egift;
    protected $request;
    protected $context;
    protected $currentPage = 1;
    
    public function __construct(
        Context $context,
        \Eleadtech\Giftpoint\Model\EgiftpointprocessFactory $egift,
        Data $helper
    ) {
        parent::__construct($context);
        $this->helper = $helper;
        $this->egift = $egift;
        $this->context = $context;
    }
    public function getConfig(){
        return $this->helper->getConfig();
    }
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($this->getGiftPointCollection()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager'
            )->setCollection(
                $this->getGiftPointCollection()
            );
            $this->setChild('giftpointpager', $pager);
            $this->getGiftPointCollection()->load();
        }
        
        $this->pageConfig->getTitle()->set(__('Gift Point'));
        return $this;
    }
    public function getPagerHtml()
    {
        return $this->getChildHtml('giftpointpager');
    }
    public function getHelper()
    {
        return $this->helper;
    }
    
    public function getTotalPoints()
    {
        return $this->helper->getTotalPoints();
    }
    
    public function getItemMessages()
    {
        return $this->helper->getItemMessages();
    }
    
    public function getQuoteMessage()
    {
        return $this->helper->getQuoteMessage();
    }
    public function setPage($page)
    {
        $this->currentPage = $page;
        return $this;
    }
    public function getPage()
    {
        return $this->currentPage;
    }
    public function getGiftPointCollection()
    {
        $page = $this->getPage();
        return $this->egift->getGiftData(false, $page);
    }
    
    public function getCustomerAvailablePoint()
    {
        return $this->egift->getCustomerAvailablePoint();
    }
    
    public function isInLogin()
    {
        return $this->helper->isInLogin();
    }
    
    public function getProductLoadingUrl()
    {
        return $this->helper->getProductLoadingUrl();
    }
    
    public function getSubmitActionUrl()
    {
        return $this->helper->getSubmitActionUrl();
    }
    
    public function getGiftRequestActionUrl()
    {
        return $this->helper->getGiftRequestActionUrl();
    }
    public function getActiveTab()
    {
        return $this->context->getRequest()->getParam("activetab", 0);
    }
    
    public function getTopUrl(){
        if($this->helper->isInLogin()){
            return $this->getGiftPointUrl();
        }
        $registerPoint = intval($this->getConfig()->getRegisterPoint()); 
        
        if(!$this->enableRegisterPoint() || $registerPoint <= 0){
            return $this->getGiftPointUrl();
        }
        else{
            $isSecure = false;
            if ($this->context->getStoreManager()->getStore()->isCurrentlySecure()) {
                $isSecure = true;
            }
            return $this->context->getUrlBuilder()->getUrl('customer/account/create', ['_secure' => $isSecure]);
        }
    }
    public function getGiftPointUrl()
    {
        return $this->helper->getGiftPointUrl();
    }
    public function getGiftPageUrl()
    {
        return $this->helper->getGiftPageUrl();
    } 
    public function enabledSendingToFriend(){
        return $this->getConfig()->enabledSendingToFriend();
    }
    
    public function enableRegisterPoint(){
        return $this->getConfig()->enableRegisterPoint();
    }
    
    public function getTopMessage(){
        if($this->helper->isInLogin()){
            return __("Your Points: %1",$this->getCustomerAvailablePoint());
        }
        else{
            
            $registerPoint = intval($this->getConfig()->getRegisterPoint());
            
            if(!$this->enableRegisterPoint() || $registerPoint <= 0){
                    return __("Your Points: %1",$this->getCustomerAvailablePoint());
            }
            return __("We are offer %1 points for new account",$this->getConfig()->getRegisterPoint());
        }
    }
}
