<?php
namespace Eleadtech\Giftpoint\Block;

use Eleadtech\Giftpoint\Helper\Data;

class Giftrequest extends\ Magento\Sales\Block\Order\History
{
    protected $_template = 'account/view/giftpoint/gift_request_tab/gift_request.phtml';
    
    public function getOrders()
    {
        
        $this->orders = parent::getOrders();
        $this->orders->addFieldToFilter("is_gift_request", 1);
        //echo $this->orders->getSelect()->__toString();
        
        return $this->orders;
    }
}
