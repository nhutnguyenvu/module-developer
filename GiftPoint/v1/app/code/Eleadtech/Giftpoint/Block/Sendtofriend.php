<?php
namespace Eleadtech\Giftpoint\Block;

use Magento\Framework\View\Element\Template;
use Eleadtech\Giftpoint\Helper\Config;
use Magento\Framework\View\Element\Template\Context;

class Sendtofriend extends Template
{
    protected $config;
    protected $context;
    public function __construct(
        Context $context,
        Config $config
    ) {
        parent::__construct($context);
        $this->config = $config;
        $this->context = $context;
    }
    
    public function isEnabled(){
        return $this->config->enabledSendingToFriend();
    }
    
    public function getFeeMessage(){
        
        $type = $this->config->getSendingType();
        $fee = $this->config->getSendingFee();
   
        if(empty($fee)){
            return "";
        }
        if($type==\Eleadtech\Giftpoint\Model\Config\Source\Feetype::PERCENTAGE){
            return __("Sending Fee: %1%",$fee);
        }
        return __("Sending Fee: %1 point(s)",$fee);
    }
    
    public function getFormAction()
    {
        $isSecure = false;
        if ($this->context->getStoreManager()->getStore()->isCurrentlySecure()) {
            $isSecure = true;
        }
        return $this->context->getUrlBuilder()->getUrl('giftpoint/index/sendpointtofriend', ['_secure' => $isSecure]);
    }
    
    public function getExistingEmailUrl()
    {
        $isSecure = false;
        if ($this->context->getStoreManager()->getStore()->isCurrentlySecure()) {
            $isSecure = true;
        }
        return $this->context->getUrlBuilder()->getUrl('giftpoint/index/checkexistingemail', ['_secure' => $isSecure]);
    }
    
   
}
