<?php
namespace Eleadtech\Giftpoint\Block;

use Eleadtech\Giftpoint\Helper\Data;

class Giftproduct extends \Magento\Catalog\Block\Product\AbstractProduct
{
    protected $helper;
    
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        Data $helper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->helper = $helper;
    }
    
    public function getProductCollectionData()
    {
        
        return $this->helper->getProductCollectionData();
    }
    
    public function getPopupImageUrl($product)
    {
        return $this->helper->resizeImage($product)->getUrl();
    }
}
