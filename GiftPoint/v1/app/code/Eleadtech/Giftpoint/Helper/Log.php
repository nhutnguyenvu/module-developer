<?php

namespace Eleadtech\Giftpoint\Helper;

use Eleadtech\Giftpoint\Logger\Logger;

class Log
{
   
    protected $logger;
    
    
    public function __construct(
        Logger $logger
    ) {
        $this->logger = $logger;
    }
    public function writeLog($message)
    {
        $this->logger->info($message);
    }
}
