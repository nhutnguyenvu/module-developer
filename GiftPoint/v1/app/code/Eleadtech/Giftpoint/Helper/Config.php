<?php
namespace Eleadtech\Giftpoint\Helper;

class Config
{
    protected $scopeConfig = null;
    
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }
    public function isEnabled()
    {
        return $this->scopeConfig->getValue('giftpoint/general/enable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        
    }
    public function enabledSendingToFriend()
    {
        return $this->scopeConfig->getValue('giftpoint/send_to_friend/enable_send_to_friend', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        
    }
    public function getSendingType()
    {
        return $this->scopeConfig->getValue('giftpoint/send_to_friend/type_fee', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        
    }
    
    public function getSendingFee()
    {
        return $this->scopeConfig->getValue('giftpoint/send_to_friend/sending_fee', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);   
    }
    
    public function enableRegisterPoint()
    {
        return $this->isEnabled() && $this->scopeConfig->getValue('giftpoint/register/enable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    public function getRegisterPoint()
    {
        return $this->scopeConfig->getValue('giftpoint/register/register_point', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
