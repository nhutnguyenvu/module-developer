<?php

namespace Eleadtech\Giftpoint\Helper;

use Eleadtech\Giftpoint\Logger\Logger;

use Magento\Framework\App\Helper\AbstractHelper;

use Eleadtech\Giftpoint\Helper\Config;

class Data extends AbstractHelper
{
    protected $cart = null;
    protected $state = null;
    protected $customerSession;
    protected $productFactory;
    protected $storeManager;
    protected $customerRepository;
    protected $stockFilter;
    protected $localeDate;
    protected $productImageHelper;
    protected $log;
    
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Framework\App\State $state,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\CatalogInventory\Helper\Stock $stockFilter,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Catalog\Helper\Image $productImageHelper,
        Config $config
    ) {
        $this->cart = $cart;
        $this->state = $state;
        $this->customerSession = $customerSession;
        $this->productFactory = $productFactory;
        $this->storeManager = $storeManager;
        $this->customerRepository = $customerRepository;
        $this->stockFilter = $stockFilter;
        $this->localeDate = $localeDate;
        $this->productImageHelper = $productImageHelper;
        $this->config = $config;
        parent::__construct($context);
    }
    
    public function getLocale()
    {
        return $this->localeDate;
    }
    public function getCustomerRepository()
    {
        return $this->customerRepository;
    }
    
    public function getStoreManager()
    {
        return $this->storeManager;
    }
    
    public function getProductFactory()
    {
        return $this->productFactory;
    }
    
    public function getTotalPoints()
    {
        $quote = $this->cart->getQuote();
        if ($quote) {
            return $quote->getTotalGiftPoint();
        }
        return 0;
    }
    
    public function getConfig(){
        return $this->config;
    }
    
    public function calculateTrasferFee($transferPoint){
        $type = $this->config->getSendingType();
        $fee = $this->config->getSendingFee();
        if(empty($fee)){
            return 0;
        }
        if($type==\Eleadtech\Giftpoint\Model\Config\Source\Feetype::PERCENTAGE){
            return intval($transferPoint * floatval($fee/100));
        }
        return intval($fee);
    }
    
    public function getTotalMessage($point)
    {
        if ($point > 0) {
            return __("You will get %1 point(s) for this order", $point);
        }
        return "";
    }
    
    public function getMessage($point)
    {
        if ($point > 0) {
            return __("You will get %1 point(s) for this item", $point);
        }
        return "";
    }
    
    public function getItemMessages()
    {
        $quote = $this->cart->getQuote();
        $messages = [];
        try {
            if ($quote) {
                $total = 0;
                $items = $quote->getAllVisibleItems();
                if (count($items) > 0 && $items) {
                    foreach ($items as $item) {
                        if (!empty($item->getGiftPoint()) > 0) {
                            $messages[$item->getId()] = $this->getMessage($item->getGiftPoint());
                        }
                    }
                }
            }
        } catch (\Exception $ex) {
            $this->helper->writeLog($ex->getMessage());
        }
        return $messages;
    }
    
    public function getQuoteMessage()
    {
        return $this->getTotalMessage($this->getTotalPoints());
    }
    
    public function isAdminArea()
    {
        
        if ($this->state->getAreaCode() == \Magento\Framework\App\Area::AREA_ADMINHTML) {
            return true;
        } else {
            return false;
        }
    }
    
    public function makeGiftCollectionToArray($collection)
    {
        $gift = [];
        if ($collection) {
            $i = 0;
            foreach ($collection as $item) {
                $gift[$i]['customer_id'] = $item->getCustomerId();
                $gift[$i]['increment_id'] = $item->getIncrementId();
                $gift[$i]['url'] = $this->createOrderDetailUrl($item->getIncrementId());
                $gift[$i]['point'] = $item->getPoint();
                $gift[$i]['status'] = \Eleadtech\Giftpoint\Model\Status::getStatusLabelById($item->getStatus());
                $gift[$i]['created_at'] = $this->localeDate->formatDate($item->getCreatedAt());
                $gift[$i]['updated_at'] = $this->localeDate->formatDate($item->getUpdatedAt());
                $gift[$i]['comment'] = $item->getComment();
                $i++;
            }
        }
        return $gift;
    }
    public function createOrderDetailUrl($incrementId)
    {
        $isSecure = false;
        if ($this->storeManager->getStore()->isCurrentlySecure()) {
            $isSecure = true;
        }
        return $this->_getUrl('giftpoint/index/orderdetail', ['_secure' => $isSecure,"increment_id"=>$incrementId]);
    }
    
    public function isInLogin()
    {
        if ($this->customerSession->isLoggedIn()) {
            return true;
        }
        return false;
    }
    
    public function getCustomerSession()
    {
        return $this->customerSession;
    }
    
    public function getCurrentCustomerId(){
        return $this->customerSession->getCustomer()->getId();
    }
    public function getProductCollectionData()
    {
        
        $collection = $this->productFactory
            ->create()->getCollection()
            ->addAttributeToSelect("*")
            ->addAttributeToFilter("type_id", "simple")
            ->addAttributeToFilter("point_for_exchange", ["gt"=>0])
            ->addAttributeToFilter("is_point_exchange_product", 1);
        
        $this->stockFilter->addInStockFilterToCollection($collection);
        return $collection;
    }
    
    public function getProductLoadingUrl()
    {
        $isSecure = false;
        if ($this->storeManager->getStore()->isCurrentlySecure()) {
            $isSecure = true;
        }
        return $this->_getUrl('giftpoint/index/product', ['_secure' => $isSecure]);
    }
    
    public function getNewAddressUrl()
    {
        $isSecure = false;
        if ($this->storeManager->getStore()->isCurrentlySecure()) {
            $isSecure = true;
        }
        return $this->_getUrl('customer/address/new', ['_secure' => $isSecure]);
    }
    public function getSubmitActionUrl()
    {
        $isSecure = false;
        if ($this->storeManager->getStore()->isCurrentlySecure()) {
            $isSecure = true;
        }
        return $this->_getUrl('giftpoint/index/createorder', ['_secure' => $isSecure]);
    }
    
    public function getGiftPointUrl()
    {
        $isSecure = false;
        if ($this->storeManager->getStore()->isCurrentlySecure()) {
            $isSecure = true;
        }
        return $this->_getUrl('giftpoint/index/index', ['_secure' => $isSecure]);
    }
    
    public function getGiftRequestActionUrl()
    {
        $isSecure = false;
        if ($this->storeManager->getStore()->isCurrentlySecure()) {
            $isSecure = true;
        }
        return $this->_getUrl('giftpoint/index/giftrequest', ['_secure' => $isSecure]);
    }
    
    public function getOrderDetailRedirectUrl()
    {
        $isSecure = false;
        if ($this->storeManager->getStore()->isCurrentlySecure()) {
            $isSecure = true;
        }
        return $this->_getUrl('giftpoint/index/giftrequest', ['_secure' => $isSecure]);
    }
    
    public function getGiftPageUrl()
    {
        $isSecure = false;
        if ($this->storeManager->getStore()->isCurrentlySecure()) {
            $isSecure = true;
        }
        return $this->_getUrl('giftpoint/index/index', ['_secure' => $isSecure]);
    }
    
    public function getCustomerAvailablePoint($customerId = false)
    {
        
        if (empty($customerId)) {
            $customerId = $this->customerSession->getCustomer()->getId();
        }
        if (empty($customerId)) {
            return 0;
        }
        $customer = $this->customerRepository->getById($customerId);
        $gifpoint = $customer->getCustomAttribute("gift_point");
        if ($gifpoint) {
            return $gifpoint->getValue();
        }
        return 0;
    }
    
    public function getTotalGiftPointBySku($skus)
    {
        $totalPoint = 0;
        if (!empty($skus)) {
            foreach ($skus as $sku) {
                $product = $this->productFactory->create()->loadByAttribute("sku", $sku);
                
                if (!$product->getIsPointExchangeProduct()) {
                    $this->log->writeLog("%1 is not Exchange Product", $product->getSku());
                    return 0;
                } else {
                    $totalPoint += $product->getPointForExchange();
                }
            }
        }
        return $totalPoint;
    }
    
    public function resizeImage($product, $imageId = "product_base_image", $width = 500, $height = null)
    {
        $resizedImage = $this->productImageHelper
                           ->init($product, $imageId)
                           ->constrainOnly(true)
                           ->keepAspectRatio(true)
                           ->keepTransparency(true)
                           ->keepFrame(false)
                           ->resize($width, $height);
        return $resizedImage;
    }
    
    public function checkExistingCustomer($email){
        $email = trim($email);
        try{
            $websiteId = $this->storeManager->getWebsite()->getWebsiteId();
            $customer = $this->customerRepository->get($email,$websiteId);
            if($customer){
                return $customer->getId();
            }
        } catch (\Exception $ex) {
            
            return false;
        }
        
        return false;
    }
    
    public function getCustomerIdByEmail($email){
        return $this->checkExistingCustomer($email);
    }
    
    public function getEmailObjectByCustomerId($customerId){
        $customer = $this->customerRepository->getById($customerId);
        $objectName = [];
        if($customer){
            
            $objectName['name'] =  $customer->getFirstname() . " " . $customer->getLastname();
            $objectName['email'] = $customer->getEmail();
        }
        return $objectName;
    }
    public function getSaleSender(){
        
        $salesEmail = $this->scopeConfig->getValue(
            'trans_email/ident_sales/email',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $salesName = $this->scopeConfig->getValue(
            'trans_email/ident_sales/name',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return ['name' =>$salesName,"email"=>$salesEmail];
    }
}
