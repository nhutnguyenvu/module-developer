<?php

namespace Eleadtech\Giftpoint\Plugin;

class QuoteItemToOrderItemPlugin
{

    public function aroundConvert(\Magento\Quote\Model\Quote\Item\ToOrderItem $subject, callable $proceed, $quoteItem, $data)
    {
        // get order item
        $orderItem = $proceed($quoteItem, $data);
        $orderItem->setGiftPoint($quoteItem->getGiftPoint());
        
        return $orderItem;
    }
}
