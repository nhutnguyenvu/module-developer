<?php

namespace Eleadtech\Giftpoint\Plugin\Block\Sales\Order;

use Eleadtech\Giftpoint\Helper\Data;

class Totals
{
    private $_order = null;
    private $_helper = null;
    
    public function __construct(
        Data $helper
    ) {
        $this->_helper = $helper;
    }
    public function afterGetOrder(\Magento\Sales\Block\Order\Totals $total, $result)
    {
        
        $this->_order = $result;
        return $result;
    }
    
    public function afterGetTotals(\Magento\Sales\Block\Order\Totals $total, $result)
    {
        if ($this->_helper->isAdminArea()) {
            $code = @$result[0]['code'];
            if ($code=="grand_total") {
                if ($this->_order->getTotalGiftPoint() > 0) {
                    $dataObject = new \Magento\Framework\DataObject(
                        [
                        'code' => 'giftpoint',
                        'strong' => false,
                        'value' => $this->_order->getTotalGiftPoint(),
                        'base_value' => $this->_order->getTotalGiftPoint(),
                        'label' => __('Gift Point'),
                        'area' => ''
                        ]
                    );
                    $dataObject->setIsFormated(true);
                    $result[] = $dataObject;
                }
            }
        } else {
            if (isset($result['grand_total'])) {
                if ($this->_order->getTotalGiftPoint() > 0) {
                    $dataObject = new \Magento\Framework\DataObject(
                        [
                            'code' => 'giftpoint',
                            'strong' => false,
                            'value' => $this->_order->getTotalGiftPoint(),
                            'base_value' => $this->_order->getTotalGiftPoint(),
                            'label' => __('Gift Point'),
                            'area' => ''
                        ]
                    );

                    $dataObject->setIsFormated(true);
                    $result['giftpoint'] = $dataObject;
                }
            }
        }
        return $result;
    }
    
    /*
    public function aroundAddTotal(\Magento\Sales\Block\Order\Totals $total, callable $proceed ,\Magento\Framework\DataObject $total,$after)
    {
        $result = $proceed($total,$after);
        
        return $result;
    }*/
}
