<?php

namespace Eleadtech\Giftpoint\Plugin\Block\Checkout\Cart\Item;

use Eleadtech\Giftpoint\Helper\Data;

class Renderer
{
    private $_cart = null;
    private $_item = null;
    private $_helper = null;
    
    public function __construct(
        \Magento\Checkout\Model\Cart $cart,
        Data $helper
    ) {
        $this->_cart = $cart;
        $this->_helper = $helper;
    }
    public function afterGetItem(\Magento\Checkout\Block\Cart\Item\Renderer $render, $result)
    {
        
        $this->_item = $result;
        return $result;
    }
    
    public function afterGetMessages(\Magento\Checkout\Block\Cart\Item\Renderer $render, $result)
    {
        
        if ($this->_item) {
            $point = $this->_item->getGiftPoint();
            if ($point > 0) {
                $result[] = ['text'=>$this->_helper->getMessage($point),"type"=>""];
            }
        }
        return $result;
    }
}
