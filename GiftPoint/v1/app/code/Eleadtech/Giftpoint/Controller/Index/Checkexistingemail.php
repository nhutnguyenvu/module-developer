<?php

namespace Eleadtech\Giftpoint\Controller\Index;

use Magento\Framework\App\Action\Context;
use \Magento\Framework\Controller\Result\JsonFactory;
use Eleadtech\Giftpoint\Helper\Data;

class Checkexistingemail extends \Magento\Customer\Controller\AbstractAccount
{
    protected $_resultJsonFactory;
    protected $helper;
    
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        Data $helper
    ) {
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->helper = $helper;
        parent::__construct($context);
    }

    public function execute()
    {
        $ajax = $this->getRequest()->isAjax();
        
        if ($ajax) {
            $result['errors'] = 0;
            $result['exist_email'] = false;
            $email = $this->_request->getParam("email");
            $customerId = $this->helper->checkExistingCustomer($email);
            if(!empty($customerId)){
                $result['errors'] = 0;
                $result['exist_email'] = true;
            }
            $jsonObject = $this->_resultJsonFactory->create();
            return $jsonObject->setData($result);
        }
        return false;
    }
}
