<?php
namespace Eleadtech\Giftpoint\Controller\Index;

use Magento\Framework\App\Action\Context;

class Sendpointtofriend extends \Magento\Customer\Controller\AbstractAccount
{
    protected $friendPoint = null;
    public function __construct(
        Context $context,
        \Eleadtech\Giftpoint\Model\FriendpointFactory $friendPoint
    ) {
        
        parent::__construct($context);
        $this->friendPoint = $friendPoint;
    }

    public function execute()
    {
        $friendEmail = $this->_request->getParam("friend_email", "");
        $pointSending = $this->_request->getParam("point_sending", "");
        $comment = $this->_request->getParam("comment", "");
        $pointSending = abs(intval($pointSending));
        $resultRedirect = $this->resultRedirectFactory->create();
        try{
            
            $result = $this->friendPoint->sendPointToFriend($friendEmail,$pointSending,$comment);
            
            if($result){
                $message = __("You havel aready sent %1 points to email %2",$pointSending,$friendEmail);
                $this->messageManager->addSuccess($message);
                $resultRedirect->setPath("giftpoint/index/index");
            }
            else{
                $message = __("Cannot submit your request. Please try another time");
                $this->messageManager->addError($message);
                $resultRedirect->setPath("giftpoint/index/index");
            }
            
        } catch (\Exception $ex) {
            $message = __("Cannot Submit Your Action. Please try another time");
            $this->messageManager->addError($message);
            $resultRedirect->setPath("giftpoint/index/index");
        }
        
        return $resultRedirect;
    }
    
   
}
