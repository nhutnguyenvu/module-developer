<?php

namespace Eleadtech\Giftpoint\Controller\Index;

use Magento\Framework\App\Action\Context;
use \Magento\Framework\Controller\Result\JsonFactory;

class Index extends \Magento\Customer\Controller\AbstractAccount
{
    protected $_resultPageFactory;
    protected $_resultJsonFactory;
    protected $config;
    
    public function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        JsonFactory $resultJsonFactory,
        \Eleadtech\Giftpoint\Helper\Config $config
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->config = $config;
        
        parent::__construct($context);
    }

    public function execute()
    {
        
        if ($this->config->isEnabled()) {
            $ajax = $this->getRequest()->isAjax();
            if ($ajax) {
                return $this->_ajaxExecute();
            } else {
                $resultPage = $this->_resultPageFactory->create();
                return $resultPage;
            }
        }
        
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath("/");
        return $resultRedirect;
    }
    
    protected function _ajaxExecute()
    {
        
        $result['errors'] = 0;
        $result['data'] = "";
        $page = $this->getRequest()->getParam("p");
        $result['data'] = $this->_view->getLayout()
            ->createBlock("Eleadtech\Giftpoint\Block\Giftpoint")
            ->setPage($page)
            ->setTemplate("account/view/giftpoint/detail_tab/detail_process.phtml")->toHtml();
        
        
        $jsonObject = $this->_resultJsonFactory->create();
        
        return $jsonObject->setData($result);
    }
}
