<?php

namespace Eleadtech\Giftpoint\Controller\Index;

use Magento\Framework\App\Action\Context;

class Submit extends \Magento\Customer\Controller\AbstractAccount
{
    protected $order = null;
    public function __construct(
        Context $context,
        \Eleadtech\Giftpoint\Model\Orders $order
    ) {
        $this->order = $order;
        parent::__construct($context);
    }

    public function execute()
    {
       
        $addressId = $this->_request->getParam("data_address_id", 0);
        $skus = $this->_request->getParam("list_submit_skues", "");
        $skus = $this->_formatSkus($skus);
        
        $result = $this->order->createOrder($skus, $addressId);
       
       
        $resultRedirect = $this->resultRedirectFactory->create();
        if (!$result) {
            $message = __("Cannot Submit Your Gift Request");
            $this->messageManager->addError($message);
            $resultRedirect->setPath("giftpoint/index/index");
        } else {
            $message = __("Your Gift Request Is In Process.");
            $this->messageManager->addSuccess($message);
            $resultRedirect->setPath("giftpoint/index/index/", ["activetab"=>2]);
        }
       
        return $resultRedirect;
    }
    
    protected function _formatSkus($skus)
    {
        
        $skus = explode("[]", $skus);
        $newSkus = [];
        
        foreach ($skus as $sku) {
            if (!empty($sku)) {
                $newSkus[] = $sku ;
            }
        }
        return $newSkus;
    }
}
