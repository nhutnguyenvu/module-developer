<?php

namespace Eleadtech\Giftpoint\Controller\Index;

use Magento\Framework\App\Action\Context;
use \Magento\Framework\Controller\Result\JsonFactory;

class Giftrequest extends \Magento\Customer\Controller\AbstractAccount
{
    protected $_resultJsonFactory;
    
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory
    ) {
        $this->_resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $ajax = $this->getRequest()->isAjax();
        
        if ($ajax) {
            $result['errors'] = 0;
            $result['data'] = "";
            $this->_request->getParam("order_increment", 0);

            $result['data'] = $this->_view->getLayout()->createBlock("Eleadtech\Giftpoint\Block\Giftrequest")
                ->setTemplate("account/view/giftpoint/gift_request_tab/gift_request.phtml")->toHtml();

            $jsonObject = $this->_resultJsonFactory->create();
            return $jsonObject->setData($result);
        }
        return false;
    }
}
