<?php

namespace Eleadtech\Giftpoint\Controller\Index;

use Magento\Framework\App\Action\Context;
use \Magento\Framework\Controller\Result\JsonFactory;

class Product extends \Magento\Customer\Controller\AbstractAccount
{
    protected $_resultJsonFactory;
    
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory
    ) {
        $this->_resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $result['errors'] = 0;
        $result['data'] = "";
        
        $result['data'] = $this->_view->getLayout()->createBlock("Eleadtech\Giftpoint\Block\Giftproduct")
            ->setTemplate("account/view/giftpoint/product_tab/product_collection.phtml")->toHtml();
        
        /*
        $result['data'] = $this->_view->getLayout()->createBlock("Eleadtech\Giftpoint\Block\Customeraddress")
            ->setTemplate("account/view/giftpoint/product_tab/customer_address.phtml")->toHtml();
        */
        $jsonObject = $this->_resultJsonFactory->create();
        
        return $jsonObject->setData($result);
    }
}
