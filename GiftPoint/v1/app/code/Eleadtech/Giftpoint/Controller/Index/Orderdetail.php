<?php

namespace Eleadtech\Giftpoint\Controller\Index;

use Magento\Framework\App\Action\Context;
use \Magento\Framework\Controller\Result\JsonFactory;

class Orderdetail extends \Magento\Customer\Controller\AbstractAccount
{
    protected $order;
    
    public function __construct(
        Context $context,
        \Magento\Sales\Api\Data\OrderInterface $order
    ) {
        
        parent::__construct($context);
        $this->order = $order;
    }

    public function execute()
    {
        
        $incrementId =  $this->_request->getParam("increment_id");
        $this->order->loadByIncrementId($incrementId);
        if ($this->order->getId()) {
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath("sales/order/view", ["order_id"=>$this->order->getId()]);
        } else {
            $resultRedirect->setPath("giftpoint/index/index");
        }
        
        return $resultRedirect;
    }
}
