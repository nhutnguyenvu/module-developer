<?php

/**
 * Butility data helper
 */
namespace Eleadtech\Giftpoint\Observer;

use Magento\Framework\Event\ObserverInterface;
use Eleadtech\Giftpoint\Helper\Data;
use Eleadtech\Giftpoint\Helper\Log;
use Magento\Framework\Exception\CouldNotSaveException;

class GiftPointProcessStatus implements ObserverInterface
{

    /**
     * @var \Magento\Framework\DataObject\Copy
     */
    private $giftPointFactory = null;
    protected $helper;
    protected $log;

    /**
     * @param \Magento\Framework\DataObject\Copy $objectCopyService
     */
    public function __construct(
        Data $helper,
        Log $log,
        \Eleadtech\Giftpoint\Model\EgiftpointprocessFactory $giftPointFactory
    ) {
        
        $this->helper = $helper;
        $this->log = $log;
        $this->giftPointFactory = $giftPointFactory;
    }
    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            $incrementId = $observer->getEvent()->getInvoice()->getOrder()->getIncrementId();
            $this->giftPointFactory->updateStatusByIncrementId($incrementId, \Eleadtech\Giftpoint\Model\Egiftpointprocess::COMPLETED);
            
            return $this;
        } catch (Exception $ex) {
            $this->log->writeLog($ex->getMessage());
            throw new CouldNotSaveException(
                __($ex->getMessage()),
                $ex
            );
        }
    }
}
