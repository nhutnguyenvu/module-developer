<?php

/**
 * Butility data helper
 */
namespace Eleadtech\Giftpoint\Observer;

use Magento\Framework\Event\ObserverInterface;

class GiftPointSubmitQuoteToOrder implements ObserverInterface
{

    /**
     * @var \Magento\Framework\DataObject\Copy
     */
    protected $objectCopyService;
    private $giftPointFactory = null;
    protected $config = null;

    /**
     * @param \Magento\Framework\DataObject\Copy $objectCopyService
     */
    public function __construct(
        \Magento\Framework\DataObject\Copy $objectCopyService,
        \Eleadtech\Giftpoint\Model\EgiftpointprocessFactory $giftPointFactory,
        \Eleadtech\Giftpoint\Helper\Config $config
    ) {
        $this->objectCopyService = $objectCopyService;
        $this->giftPointFactory = $giftPointFactory;
        $this->config = $config;
    }
    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->config->isEnabled()) {
            $order = $observer->getEvent()->getData('order');
            $quote = $observer->getEvent()->getData('quote');

            if (!$quote->getIsGiftRequest()) {
                $this->objectCopyService->copyFieldsetToTarget('sales_convert_quote', 'to_order', $quote, $order);
                if ($order->getTotalGiftPoint() > 0) {
                    $this->giftPointFactory->createGiftPointByOrder($order);
                }
            } else {
                $order->setIsGiftRequest(1);
                $order->setTotalExchangedPoint($quote->getTotalExchangedPoint());
            }
        }
        
        return $this;
    }
}
