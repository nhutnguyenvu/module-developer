<?php

/**
 * Butility data helper
 */
namespace Eleadtech\Giftpoint\Observer;

use Magento\Framework\Event\ObserverInterface;
use Eleadtech\Giftpoint\Helper\Data;
use Eleadtech\Giftpoint\Helper\Log;
use Eleadtech\Giftpoint\Helper\Config;

class AddPointToSuccessAccount implements ObserverInterface
{

    /**
     * @var \Magento\Framework\DataObject\Copy
     */
    protected $helper;
    protected $log;

    /**
     * @param \Magento\Framework\DataObject\Copy $objectCopyService
     */
    public function __construct(
        Data $helper,
        Log $log
    ) {
        
        $this->helper = $helper;
        $this->log = $log;
    }
    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try{
            $enable = $this->getConfig()->enableRegisterPoint();
            $customer = $observer->getEvent()->getData('customer');
            if($enable){
                $rewardPoint = $this->getConfig()->getRegisterPoint();
                $rewardPoint = intval($rewardPoint);
                if($rewardPoint > 0){
                    $customer->setCustomAttribute('gift_point', $rewardPoint);
                    $this->helper->getCustomerRepository()->save($customer);
                }

            }
        } catch (\Exception $ex) {
            $this->log->writeLog($ex->getMessage());
        }
        
        return $this;
    }
    public function getConfig(){
        return $this->helper->getConfig();
    }
}
