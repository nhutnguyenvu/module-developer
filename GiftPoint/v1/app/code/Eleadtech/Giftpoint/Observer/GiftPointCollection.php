<?php

/**
 * Butility data helper
 */
namespace Eleadtech\Giftpoint\Observer;

use Magento\Framework\Event\ObserverInterface;
use Eleadtech\Giftpoint\Helper\Data;
use Eleadtech\Giftpoint\Helper\Log;
use Eleadtech\Giftpoint\Helper\Config;

class GiftPointCollection implements ObserverInterface
{

    /**
     * @var \Magento\Framework\DataObject\Copy
     */
    protected $productFactory;
    protected $helper;
    protected $log;
    

    /**
     * @param \Magento\Framework\DataObject\Copy $objectCopyService
     */
    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory,
        Data $helper,
        Log $log
    ) {
        $this->productFactory = $productFactory;
        $this->helper = $helper;
        $this->log = $log;
    }
    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
       
        $quote = $observer->getEvent()->getData('quote');
        $this->_calculateTotalGift($quote);
        
        return $this;
    }
    public function getConfig(){
        return $this->helper->getConfig();
    }
    protected function _calculateTotalGift($quote)
    {
        if ($this->getConfig()->isEnabled()) {
            try {
                if ($quote) {
                    if (!$quote->getIsGiftRequest()) {
                        $total = 0;
                        $items = $quote->getAllVisibleItems();
                        if (count($items) > 0 && $items) {
                            foreach ($items as $item) {
                                if ($item->getParentItemId()) {
                                    continue;
                                }

                                $product = $this->productFactory->create();
                                $product->load($item->getProduct()->getId());
                                $point = $product->getPointForPurchase();

                                if (!empty($point)) {
                                    $item->setGiftPoint($point * intval($item->getQty()));
                                    $total += $point * intval($item->getQty());
                                }
                            }
                            $quote->setTotalGiftPoint($total);
                            $quote->save();
                        }
                    }
                }
            } catch (\Exception $ex) {
                $this->log->writeLog($ex->getMessage());
            }
        }
       
        return $this;
    }
}
