<?php
/**
 * Copyright Â© 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eleadtech\Giftpoint\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        //add gift point table
        if (!$setup->tableExists('egift_point_process')) {
            $table = $setup->getConnection()->newTable(
                $setup->getTable('egift_point_process')
            )
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Id'
            )
            ->addColumn(
                'customer_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned'=>true, 'nullable'=>false, 'default' => '0'],
                'Customer Id'
            )

            ->addColumn(
                'increment_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [],
                'Increment Id'
            )

            ->addColumn(
                'status',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                255,
                [],
                'Status'
            )
            ->addColumn(
                'point',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                255,
                [],
                'Gift Point'
            )

            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                null,
                [],
                'create_at'
            )

            ->addForeignKey( // Add foreign key for table entity
                $setup->getFkName(
                    'egift_point_process', // New table
                    'customer_id', // Column in New Table
                    'customer_entity', // Reference Table
                    'entity_id' // Column in Reference table
                ),
                'customer_id', // New table column
                $setup->getTable('customer_entity'), // Reference Table
                'entity_id', // Reference Table Column
                // When the parent is deleted, delete the row with foreign key
                Table::ACTION_CASCADE
            );

            $connection = $setup->getConnection();
            $connection->createTable($table);
        }
        
        $setup->endSetup();
    }
}
