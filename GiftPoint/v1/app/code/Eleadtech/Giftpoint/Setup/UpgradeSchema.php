<?php

/**
 * Copyright Â© 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eleadtech\Giftpoint\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        
        $installer = $setup;
        
        if (version_compare($context->getVersion(), '1.0.5', '<')) {
            $installer->getConnection()->addColumn(
                $installer->getTable('egift_point_process'),
                'updated_at',
                [
                'type'=>\Magento\Framework\DB\Ddl\Table::TYPE_DATETIME ,
                'length' => null,
                'nullable' => true,
                'comment' => 'Updated At',
                    ]
            );
            $installer->getConnection()->addColumn(
                $installer->getTable('egift_point_process'),
                'comment',
                [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => null,
                'nullable' => true,
                'comment' => 'Comment',
                    ]
            );
        }
        if (version_compare($context->getVersion(), '1.0.8', '<')) {
            
            $table = $setup->getConnection()->newTable(
                $setup->getTable('egift_point_friend_process')
            )
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Id'
            )
            ->addColumn(
                'sender_customer_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned'=>true, 'nullable'=>false, 'default' => '0'],
                'Sender'
            )

            ->addColumn(
                'receiver_customer_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                 ['unsigned'=>true, 'nullable'=>false, 'default' => '0'],
                'Receiver'
            )

            ->addColumn(
                'website_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                5,
                ['unsigned'=>true, 'nullable'=>false, 'default' => '0'],
                'Website Id'
            )
            ->addColumn(
                'point_transfer',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                255,
                [],
                'Point Transder'
            )
                
            ->addColumn(
                'fee',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                255,
                [],
                'Fee'
            )

            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                null,
                [],
                'create_at'
            )

            ->addForeignKey( // Add foreign key for table entity
                $setup->getFkName(
                    'egift_point_friend_process', // New table
                    'sender_customer_id', // Column in New Table
                    'customer_entity', // Reference Table
                    'entity_id' // Column in Reference table
                ),
                'sender_customer_id', // New table column
                $setup->getTable('customer_entity'), // Reference Table
                'entity_id', // Reference Table Column
                // When the parent is deleted, delete the row with foreign key
                Table::ACTION_CASCADE
            )
            ->addForeignKey( // Add foreign key for table entity
                $setup->getFkName(
                    'egift_point_friend_process', // New table
                    'receiver_customer_id', // Column in New Table
                    'customer_entity', // Reference Table
                    'entity_id' // Column in Reference table
                ),
                'receiver_customer_id', // New table column
                $setup->getTable('customer_entity'), // Reference Table
                'entity_id', // Reference Table Column
                // When the parent is deleted, delete the row with foreign key
                Table::ACTION_CASCADE
            )
            
            ->addForeignKey( // Add foreign key for table entity
                $setup->getFkName(
                    'egift_point_friend_process', // New table
                    'website_id', // Column in New Table
                    'store_website', // Reference Table
                    'website_id' // Column in Reference table
                ),
                'website_id', // New table column
                $setup->getTable('store_website'), // Reference Table
                'website_id', // Reference Table Column
                // When the parent is deleted, delete the row with foreign key
                Table::ACTION_CASCADE
            );

            $connection = $setup->getConnection();
            $connection->createTable($table);
        }
        if (version_compare($context->getVersion(), '1.0.9', '<')) {
            
            $installer->getConnection()->addColumn(
                $installer->getTable('egift_point_friend_process'),
                'comment',
                [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => null,
                'nullable' => true,
                'comment' => 'Comment',
                    ]
            );
           
        }
        $installer->endSetup();
    }
}
