<?php

/**
 * Copyright Â© 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Eleadtech\Giftpoint\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;

/*use Magento\Framework\Setup\SchemaSetupInterface;*/


/**
 * @codeCoverageIgnore
 */
class UpgradeData implements UpgradeDataInterface
{

 
    private $customerSetupFactor;
    
    public function __construct(
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory,
        \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactor
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->customerSetupFactor = $customerSetupFactor;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        
        $installer = $setup;
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $customerSetup = $this->customerSetupFactor->create(['setup' => $setup]);
        $installer->startSetup();
        
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $attributeTexts1 = [
                'point_for_purchase' => 'Point for Reward',
            ];
            foreach ($attributeTexts1 as $key => $value) {
                $eavSetup->addAttribute(\Magento\Catalog\Model\Product::ENTITY, $key, [
                    'group' => 'Gift Point',
                    'label' => $value,
                    'type' => 'varchar',
                    'input' => 'text',
                    'frontend' => '',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'searchable' => true,
                    'filterable' => false,
                    'filterable_in_search' => true,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'visible_in_advanced_search' => false,
                    'used_in_product_listing' => true,
                    'unique' => false
                ]);
            }
            $attributeYesNo = [
                'is_point_exchange_product' => 'Use This Product For Exchange',
            ];
            foreach ($attributeYesNo as $key => $value) {
                $eavSetup->addAttribute(\Magento\Catalog\Model\Product::ENTITY, $key, [
                    'group' => 'Gift Point',
                    'label' => $value,
                    'type' => 'int',
                    'input' => 'boolean',
                    'frontend' => '',
                    'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'searchable' => true,
                    'filterable' => true,
                    'filterable_in_search' => true,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'visible_in_advanced_search' => false,
                    'unique' => false,
                    'used_in_product_listing' => true,
                    'apply_to' => 'simple'
                    
                ]);
            }
            
            $attributeTexts = [
                'point_for_exchange' => 'Point For Exchange'
            ];
            foreach ($attributeTexts as $key => $value) {
                $eavSetup->addAttribute(\Magento\Catalog\Model\Product::ENTITY, $key, [
                    'group' => 'Gift Point',
                    'label' => $value,
                    'type' => 'varchar',
                    'input' => 'text',
                    'frontend' => '',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'searchable' => true,
                    'filterable' => false,
                    'filterable_in_search' => true,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'visible_in_advanced_search' => false,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to' => 'simple'
                ]);
            }
        }
        
        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $eavSetup->addAttribute(
                'customer',
                'gift_point',
                [
                'label' => 'Gift Point',
                'type' => 'text',
                'frontend_input' => 'text',
                'required' => false,
                'visible' => true,
                'system'=> 0,
                'position' => 105,
                ]
            );

            $loyaltyAttribute = $customerSetup->getEavConfig()->getAttribute('customer', 'gift_point');
            $loyaltyAttribute->setData('used_in_forms', ['adminhtml_customer']);
            $loyaltyAttribute->save();
        }
        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $connection = $setup->getConnection();
            
            $extraColumnsData [$setup->getTable('quote_item')] = [
                'gift_point' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '12,0',
                    'nullable' => false,
                    'comment' => 'Gift Point',
                ]
               
            ];
            
            
            $extraColumnsData [$setup->getTable('quote')] = [
                'total_gift_point' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '12,0',
                    'nullable' => false,
                    'comment' => 'Total Gift Point',
                ]
                
            ];
            
            $extraColumnsData [$setup->getTable('sales_order_item')] = [
                'gift_point' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '12,0',
                    'nullable' => false,
                    'comment' => 'Gift Point',
                ],
            ];
            
            
            $extraColumnsData [$setup->getTable('sales_order')] = [
                'total_gift_point' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '12,0',
                    'nullable' => false,
                    'comment' => 'Total Gift Point',
                ],
                
            ];

            foreach ($extraColumnsData as $table => $columns) {
                foreach ($columns as $name => $definition) {
                    $connection->addColumn($table, $name, $definition);
                }
            }
        }
        if (version_compare($context->getVersion(), '1.0.6', '<')) {
            $connection = $setup->getConnection();
            $extraColumnsData [$setup->getTable('quote')] = [
                'is_gift_request' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                     null,
                    'nullable' => false,
                    'default' => 0,
                    'comment' => 'Gift Request',
                ]
                
            ];
            
            $extraColumnsData [$setup->getTable('sales_order')] = [
                'is_gift_request' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                     null,
                    'nullable' => false,
                    'default' => 0,
                    'comment' => 'Gift Request',
                ],
                
            ];
            
            foreach ($extraColumnsData as $table => $columns) {
                foreach ($columns as $name => $definition) {
                    $connection->addColumn($table, $name, $definition);
                }
            }
        }
        
        if (version_compare($context->getVersion(), '1.0.7', '<')) {
            $connection = $setup->getConnection();
            $extraColumnsData [$setup->getTable('quote')] = [
                'total_exchanged_point' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                     null,
                    'nullable' => false,
                    'default' => 0,
                    'comment' => 'Total Exchanged Point',
                ]
                
            ];
            
            $extraColumnsData [$setup->getTable('sales_order')] = [
                'total_exchanged_point' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                     null,
                    'nullable' => false,
                    'default' => 0,
                    'comment' => 'Total Exchanged Point',
                ],
                
            ];
            
            foreach ($extraColumnsData as $table => $columns) {
                foreach ($columns as $name => $definition) {
                    $connection->addColumn($table, $name, $definition);
                }
            }
        }
        $installer->endSetup();
    }
}
