/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'jquery',
        'uiComponent',
        'ko',
        'Eleadtech_Giftpoint/js/model/mgiftpoint',
        'mage/url'
    ],
    function ($, Component,ko,point,urlBuilder) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Eleadtech_Giftpoint/account/giftpoint/detail'
            },
            
            pointData: point.getData(),
            
            getGiftPointData: function(){
                return this.pointData.list;
            },
            
            getAvailablePoint: function(){
                return this.pointData.total;
            },
            getOrderView: function(orderId){
                return urlBuilder.build("sales/order/view/order_id/" + orderId);
            }
            
        });
    }
);

