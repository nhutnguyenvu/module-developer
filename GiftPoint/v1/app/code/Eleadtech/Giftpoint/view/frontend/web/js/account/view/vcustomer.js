/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'jquery',
        'uiComponent',
        'ko',
        'Eleadtech_Giftpoint/js/model/mcustomer-addresses'
    ],
    function ($,Component,ko,customerAddress) {
        'use strict';
        
        return Component.extend({
            
            defaults: {
                template: {
                    name: 'Eleadtech_Giftpoint/account/customer-address.html'
                }
            },
            
            addresses: customerAddress.getAddressItems(),
            selectedAddressEle: "#selectedAddress",
           
            initLoading: function () {
                var self = this;
                //console.log("ssssssssssss");
                $(".customer_address ul").off("mouseover","li").on("mouseover","li", function () {
                    $(this).closest("ul").find("li").removeClass("active");
                    $(this).addClass('active');
                   
                    
                });
                
                $(".customer_address ul").find("li").each(function () {
                    var addressId = $(this).attr("address_id");
                    self.hideButtonRemoveSelection(addressId);
                });
                
            },
            
            isVisible: function () {
                return true;
            },
            
            hideButtonRemoveSelection: function (addressId) {
                var liobject =  "[address_id=" + addressId + "]";
                $(liobject).find("a.remove_selection").hide();
            },
            
            showButtonRemoveSelection: function (addressId) {
                var liobject =  "[address_id=" + addressId + "]";
                $(liobject).find("a.remove_selection").show();
            },
           
            selectAddress: function () {
                 //in loop, so this = model/customer/address (get from loop)
                $("#selectedAddress").val(this.customerAddressId);
                var liobject =  "[address_id=" + this.customerAddressId + "]";
                $(liobject).closest("ul").find("li").removeClass("selected");
                //remove all 'remove selection' button
                $(liobject).closest("ul").find("li").find("a.remove_selection").hide();
                
                $(liobject).addClass("selected");
                $(liobject).find("a.remove_selection").show();
                
            },
            getSelectedAddressId: function () {
                $(selectedAddressEle).val();
            },
            
            removeSelection: function () {
               var liobject =  "[address_id=" + this.customerAddressId + "]";
               $(liobject).removeClass("selected");
            }
        });
    }
);

