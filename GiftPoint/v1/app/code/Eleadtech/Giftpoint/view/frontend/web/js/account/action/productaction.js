/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    ['ko','jquery',
        'Eleadtech_Giftpoint/js/account/action/loadproduct',
        'Eleadtech_Giftpoint/js/model/mgiftpoint',
        'mage/translate']
    ,
    function (ko, $, loadProductAction,mpoint) {
        'use strict';
        
        return {
            
            exchangePointAttr: "[name='data_exchange_point']",
            exchangeSelecedPoint: ".exchange_point span",
            productContainer: ".gift_collection ol",
            sumitSkuAttr: "[name='submit_sku']",
            sumitListSkuAttr: "[name='list_submit_skues']",
            selectedAddressEle: "[name=data_address_id]",
            submitButtonAttr: "[name=submit_button]",
            
            loadProductCollection: function() {
                var self = this;
                loadProductAction(self);
            },
            initLoading: function(){
                
                $(this.productContainer).off("mouseover","li").on("mouseover","li", function(){
                    $(this).closest("ol").find("li").removeClass("active");
                    $(this).addClass('active');
                });
                
                var self = this;
                $(this.productContainer).off("click","li").on("click","li", function(){
                    $(this).toggleClass('selected');
                    if(!self.applySelectedPoint()){
                        $(this).removeClass('selected');
                        self.getMessageReport();
                    }
                    self.saveSubmitSkus();
                });
                
                $("#giftpoint").off("click",self.submitButtonAttr).on("click",self.submitButtonAttr, function(){
                    return self.validate();
                });
                
            },
            
            getLiObjectElements: function(){
                return $(this.productContainer).find("li");
            },
            
            getLiObjectSelectedElements: function(){
                return $(this.productContainer).find("li.selected");
            },
            calculateExchangePoint: function(){
                var totalPoint = 0;
                var self = this;
                
                this.getLiObjectSelectedElements().each(function(){
                    var oldPoint = totalPoint;
                    totalPoint = totalPoint + parseFloat($(this).find(self.exchangePointAttr).val());
                });
                return totalPoint;
            },
            
            applySelectedPoint: function(){
                
                var exchangePoint = this.calculateExchangePoint();
                var result = false;
                if(mpoint.getAvailablePoint() >= exchangePoint){
                    $(this.exchangeSelecedPoint).html(exchangePoint);
                    result = true;
                }
                
                
                return result;
            },
            
            getMessageReport: function(){
                return alert($.mage.__("Your point is not enough"));
            },
            saveSubmitSkus: function(){
                var self = this;
                var submitSkus = "";
                this.getLiObjectSelectedElements().each(function(){
                    submitSkus = submitSkus + $(this).find(self.sumitSkuAttr).val() + "[]";
                });
                $(this.sumitListSkuAttr).val(submitSkus);
            },
            
            sumitCollection: function(){
                return this.validate();
            },
            
            validate: function(){
                try{
                    var addressId = $(this.selectedAddressEle).val();
                    addressId = parseInt(addressId);
                    if(isNaN(addressId) || addressId <= 0){
                        alert($.mage.__("Please choose your address"));
                        return false;
                    }
                }
                catch(ex){
                    alert($.mage.__("Please choose your address"));
                    return false;
                }
                
                try{
                    var sku = $(this.sumitListSkuAttr).val();
                    if(sku == ""){
                        alert($.mage.__("Please choose your gift(s)"));
                        return false;
                    }
                    
                }
                catch(ex){
                    alert($.mage.__("Please choose your gift(s)"));
                    return false;
                }
                return true;
                
            }
        };
    }
);
