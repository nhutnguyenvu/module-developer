/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    [
        'jquery',
        'Eleadtech_Giftpoint/js/model/mgiftpoint'
    ],
    function ($, giftpoint) {
        'use strict';

        return function (skus,address_id) {
            
            $('#giftpoint').trigger('processStart');
            
            $.post({
                    url: giftpoint.submitCollectionUrl(),
                    data: {skus: skus,address_id: address_id}
                }
            ).done(
                function (response) {
                    if (response.errors == false) {
                        
                    } else {
                        
                    }
                    $('#giftpoint').trigger('processStop');
                   
                }.bind(this)
            ).fail(
                function (response) {
                    $('#giftpoint').trigger('processStop');
                }.bind(this)
            );
            return true;
        };
    }
);
