/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    [
        'jquery',
        'Eleadtech_Giftpoint/js/account/action/giftrequest/loadgiftrequest',
        'mage/translate'
    ],
    function ($,loadGiftRequestAction) {
        'use strict';
        
        return {
            giftRequestContainer: "#tabs-request-gift",
            
            initLoading: function(){
                var self = this;
                $(this.giftRequestContainer).find(".pages").off("click","a.page,a.action").on("click","a.page,a.action", function(event){
                    event.preventDefault();
                    var url = $(this).attr("href");
                    self.loadGiftRequestCollection(url);
                });
            },
            loadGiftRequestCollection: function(url) {
                loadGiftRequestAction(url,this);
            }
        };
    }
);
