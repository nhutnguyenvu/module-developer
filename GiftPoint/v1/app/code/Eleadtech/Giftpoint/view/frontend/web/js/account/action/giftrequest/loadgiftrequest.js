/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    [
        'jquery',
        'Eleadtech_Giftpoint/js/model/mgiftpoint'
    ],
    function ($, giftpoint) {
        'use strict';

        return function (url,loadRequest) {
            
            
            var urlRequest = url;
            if(urlRequest === "" || urlRequest === undefined){
                urlRequest = giftpoint.getGiftRequestUrl();
            }
            $('#giftpoint').trigger('processStart');
            $.ajax({
                    url: urlRequest,
                    type: "POST"
                }
            ).done(
                function (response) {
                    if (response.errors == false) {
                        var container = giftpoint.getGiftRequestContainer();
                        container.html(response.data);
                        if(loadRequest != undefined){
                            loadRequest.initLoading();
                        }
                    } else {
                        
                    }
                    $('#giftpoint').trigger('processStop');
                   

                }.bind(this)
            ).fail(
                function (response) {
                    $('#giftpoint').trigger('processStop');
                }.bind(this)
            );
            return true;
        };
    }
);
