/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    [
        'jquery',
        'mage/translate'
    ],
    function ($, giftpoint) {
        'use strict';

        return function (email,mainObject,submit) {
            
            $('#giftpoint').trigger('processStart');
            var existEmail = false;
            
                
            $.ajax({
                url: window.checkExistingEmailUrl,
                type: "POST",
                async: true,
                data:{email:email}
            }
            ).always(function(){ // or use complete option of ajax request

            }).done(
                function (response) {
                    if (response.errors == false) {
                        existEmail = response.exist_email;
                        mainObject.setIsExistingEmail(existEmail);
                        if(submit && existEmail){
                            mainObject.submit();
                        }
                        else{
                            alert($.mage.__("Your friend email must be account in this website."));
                        }

                    } else {
                       
                    }
                    $('#giftpoint').trigger('processStop');


                }.bind(this)
            ).fail(
                function (response) {
                    $('#giftpoint').trigger('processStop');
                }.bind(this)
            );
            return true;
        };
    }
);

