/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    [
        'jquery',
        'Eleadtech_Giftpoint/js/model/mgiftpoint'
    ],
    function ($, giftpoint) {
        'use strict';

        return function (productAction) {
            
            $('#giftpoint').trigger('processStart');
            
            $.ajax({
                    url: giftpoint.getProductLoadingUrl(),
                    type: "POST"
                }
            ).done(
                function (response) {
                    if (response.errors == false) {
                        var container = giftpoint.getProductApllyContainer();
                        container.html(response.data);
                        if(productAction != undefined ){
                            productAction.initLoading();
                        }
                        
                    } else {
                        
                    }
                    $('#giftpoint').trigger('processStop');
                   

                }.bind(this)
            ).fail(
                function (response) {
                    $('#giftpoint').trigger('processStop');
                }.bind(this)
            );
            return true;
        };
    }
);
