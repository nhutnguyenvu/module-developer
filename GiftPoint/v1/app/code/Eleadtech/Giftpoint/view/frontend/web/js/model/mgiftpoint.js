/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    ['ko','jquery'],
    function (ko,$) {
        'use strict';  
        return {
            getProductLoadingUrl: function(){
                return window.productLoadingUrl;
            },
            
            getGiftRequestUrl: function(){
                return window.giftRequestUrl;
            },
            
            getGiftPageUrl: function(){
                return window.giftPageUrl;
            },
            getGiftPageContainer(){
                return $("#tabs-detail");
            },
            getProductApllyContainer(){
                return $(".gift_collection");
            },
            
            getGiftRequestContainer(){
                return $("#tabs-request-gift");
            },
            
            getAvailablePoint: function(){
                return parseFloat(window.getCustomerAvailablePoint);
            }
        };
    }
);
