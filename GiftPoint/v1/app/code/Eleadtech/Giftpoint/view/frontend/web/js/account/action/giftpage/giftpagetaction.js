/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    [
        'jquery',
        'Eleadtech_Giftpoint/js/account/action/giftpage/loadgiftpage'
    ],
    function ($,loadGiftPageAction) {
        'use strict';
        
        return {
            giftRequestContainer: "#tabs-detail",
            
            initLoading: function(){
                var self = this;
                $(this.giftRequestContainer).find(".pages").off("click","a.page,a.action").on("click","a.page,a.action", function(event){
                    event.preventDefault();
                    var url = $(this).attr("href");
                    self.loadGiftPageCollection(url);
                });
            },
            loadGiftPageCollection: function(url) {
                loadGiftPageAction(url,this);
            }
        };
    }
);
