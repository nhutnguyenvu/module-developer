/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    [
        'jquery'
    ],
    function ($) {
        'use strict';
        
        return {
            getTabNameOnUrl: function () {
               var fullLink = window.location.href;
               var array = fullLink.split("#");
               if (array.length > 1) {
                   return "#" + array[1];
               }
               return false;
            }
        };
    }
);

