/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'jquery',
        'uiComponent',
        'ko',
        'Eleadtech_Giftpoint/js/model/mcustomer-addresses',
        'mage/translate'
    ],
    function ($,Component,ko,customerAddress) {
        'use strict';
        
        return Component.extend({
            
            defaults: {
                template: {
                    name: 'Eleadtech_Giftpoint/account/customer-address.html'
                }
            },
            
            addresses: customerAddress.getAddressItems(),
            selectedAddressEle: "[name=data_address_id]",
           
            initLoading: function () {
                var self = this;
                //console.log("ssssssssssss");
                $(".customer_address ul").off("mouseover","li").on("mouseover","li", function () {
                    $(this).closest("ul").find("li").removeClass("active");
                    $(this).addClass('active');
                });
                
                $(".customer_address ul").off("mouseleave","li").on("mouseleave","li", function () {
                    $(this).removeClass('active');
                });
                
                $(".customer_address ul").find("li").each(function () {
                    var addressId = $(this).attr("address_id");
                });
                
            },
            
            isVisible: function () {
                return true;
            },
            
            selectAddress: function () {
                 //in loop, so this = model/customer/address (get from loop)
                
                $("[name=data_address_id]").val(this.customerAddressId);
                var liobject =  "[address_id=" + this.customerAddressId + "]";
                $(liobject).closest("ul").find("li").removeClass("selected");
                //remove all 'remove selection' button
                $(liobject).addClass("selected");
                
                
            },
            getSelectedAddressId: function () {
                $(this.selectedAddressEle).val();
            },
            
            removeSelection: function () {
               var liobject =  "[address_id=" + this.customerAddressId + "]";
               $(liobject).removeClass("selected");
            },
            
            getNewAddressUrl: function () {
                return window.newAddressUrl;
            },
            
            getNonAddressMessage: function () {
                return $.mage.__("Please create your address before submit your gifts");
            }
        });
    }
);

