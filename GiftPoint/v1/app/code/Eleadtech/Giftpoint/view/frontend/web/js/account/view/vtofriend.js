/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'jquery',
        'uiComponent',
        'Eleadtech_Giftpoint/js/account/action/sendtofriend/checkExistingEmail',
        'mage/validation',
        'mage/translate'
    ],
    function ($,Component,checkExistingEmailAction) {
        'use strict';
        
        return Component.extend({
            
            defaults: {
                template: {
                    name: 'Eleadtech_Giftpoint/account/sendtofriend.html'
                }
            },
            
            
            isExistingEmail: false,
            formObj: null,
            inProgress: false,
            initialize: function () {
                this.formObj = '[data-role=pointsending]';
                return this._super();
            },
            
            
            isVisible: function() {
                return window.enabledSendingToFriend;
            },
            
            setIsExistingEmail: function(value){
                this.isExistingEmail = value;
            },
            
            sendpoint: function(sendingForm){
                
                if(this.isExistingEmail){
                    return true;
                }
                else{
                    var confirmVal = confirm($.mage.__("Are you sure ?"));
                    if(confirmVal){
                        var sendingData = {},
                        formDataArray = $(sendingForm).serializeArray();
                        formDataArray.forEach(function (entry) {
                            sendingData[entry.name] = entry.value;
                        });

                        this.makeButtonStatus(true);
                        if($(sendingForm).validation()
                            && $(sendingForm).validation('isValid')
                        ) {

                            var pointSending = parseFloat(sendingData["point_sending"]);

                            if(pointSending > this.getAvailablePoint()){
                                alert($.mage.__("Your points is not available enough")); 
                                this.makeButtonStatus(false);
                                return false;
                            }
                            if(!this.isExistingEmail){
                                this.checkExistingEmailAndSumit(sendingData["friend_email"]);
                            }
                            //window.setTimeout("this.submit",5000);
                        }
                        this.makeButtonStatus(false);
                        return false;
                    }
                    
                }
                
            },
            
            getFormAction: function(){
                return window.formAction;
            },
            
            getFeeMessage: function(){
                return window.feeMessage;
            },
            
            getAvailablePoint: function(){
                return parseFloat(window.getCustomerAvailablePoint);
            }, 
            
            makeButtonStatus: function(status){
                $(this.formObj).find("button").prop("disabled",status);
                //$(this.formObj).find("button").hide();
            },
           
            checkExistingEmailAndSumit: function(email){
                checkExistingEmailAction(email,this,true);
            },
            
            submit: function(){
                $(this.formObj).submit();
            }
            
        });
    }
);
