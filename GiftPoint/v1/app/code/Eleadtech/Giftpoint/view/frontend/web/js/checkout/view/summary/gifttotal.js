/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'jquery',
        'uiComponent'
    ],
    function ($, Component) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Eleadtech_Giftpoint/checkout/quote_message'
            },
            
            getQuoteMessage: function () {
                return window.quote_message;
            }
        });
    }
);

