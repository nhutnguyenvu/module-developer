<?php

namespace Eleadtech\Eshpg\Model\ResourceModel\Eshpg;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Eleadtech\Eshpg\Model\Eshpg', 'Eleadtech\Eshpg\Model\ResourceModel\Eshpg');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }
}
