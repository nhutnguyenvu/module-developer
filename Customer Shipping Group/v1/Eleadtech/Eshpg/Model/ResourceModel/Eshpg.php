<?php
namespace Eleadtech\Eshpg\Model\ResourceModel;

class Eshpg extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('egrouppaymentshipping', 'id');
    }
}
