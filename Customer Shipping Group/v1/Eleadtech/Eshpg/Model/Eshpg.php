<?php
namespace Eleadtech\Eshpg\Model;

class Eshpg extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Eleadtech\Eshpg\Model\ResourceModel\Eshpg');
    }
    
    public function checkExistingGroupIdInWebsites($groupId, $websiteIds, $id = false)
    {
        
        $collection = $this->getCollection()
            ->addFieldToFilter("group_id", $groupId);

        $whereString = "";
        if (!empty($websiteIds)) {
            $n = count($websiteIds);
            for ($i = 0; $i < $n; $i++) {
                if ($i == 0) {
                    $whereString .= " FIND_IN_SET ($websiteIds[$i], website_ids)";
                } else {
                    $whereString .= " OR FIND_IN_SET ($websiteIds[$i], website_ids)";
                }
            }
        }

        if (empty($id)) {
            $id = 0;
        }
        
        if (!empty($whereString)) {
            $collection->getSelect()
                ->where($whereString)
                ->where("id!=$id");
        }
        
        if ($collection->count() > 0){
            return true;
        }
        
        return false;
    }

    public function loadByGroupId($groupId, $websiteId)
    {

        $collection = $this->getCollection()
            ->addFieldToFilter("group_id", $groupId);

        $collection->getSelect()->where("FIND_IN_SET($websiteId,website_ids)")->limit(1);

        $item = $collection->getFirstItem();

        if ($item) {
            return $item;
        }
        return false;
    }
}
