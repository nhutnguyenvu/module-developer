<?php
namespace Eleadtech\Eshpg\Plugin\Model\Quote;

use Magento\Store\Model\StoreManagerInterface;

class ShippingMethodManagement
{

    private $eshpg;
    private $helperData;
    private $cart;
    private $sessionQuote;

    public function __construct(
        \Eleadtech\Eshpg\Helper\Data $_helperData,
        \Eleadtech\Eshpg\Model\EshpgFactory $eshpg,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Backend\Model\Session\Quote $sessionQuote
    ) {

        $this->eshpg = $eshpg;
        $this->helperData = $_helperData;
        $this->cart = $cart;
        $this->sessionQuote = $sessionQuote;
    }

    public function afterEstimateByExtendedAddress(\Magento\Quote\Model\ShippingMethodManagement $shippingMethodManagement, $result)
    {
        return $this->filterShippingMethodByCustomerGroup($result);
    }

    public function afterEstimateByAddressId(\Magento\Quote\Model\ShippingMethodManagement $shippingMethodManagement, $result)
    {
        return $this->filterShippingMethodByCustomerGroup($result);
    }

    protected function filterShippingMethodByCustomerGroup($result)
    {
        try {
            if ($this->helperData->isEnabled()) {
                $adminPage = $this->helperData->isAdminPage();

                if ($adminPage) {
                    $quote = $this->sessionQuote->getQuote();
                } else {
                    $quote = $this->cart->getQuote();
                }

                $storeId = $quote->getStoreId();

                $store = $this->helperData->getStoreManager()->getStore($storeId);

                $websiteId = $store->getWebsiteId();

                $currentGroupId = $quote->getCustomerGroupId();

                $eshpgObj = $this->eshpg->create()->loadByGroupId($currentGroupId, $websiteId);

                if (!$eshpgObj) {
                    return $result;
                } else {
                    $shippingCodes = $eshpgObj->getShippingCodes();

                    $newResult = [];
                    if (!empty($shippingCodes)) {
                        $shippingCodesArr = explode(",", $shippingCodes);
                        if (!empty($result)) {
                            foreach ($result as $key => $shippingMethodModel) {
                                $code = $shippingMethodModel->getCarrierCode();
                                if (in_array($code, $shippingCodesArr)) {
                                    $newResult[$key] = $shippingMethodModel;
                                }
                            }
                            return $newResult;
                        }
                    }
                }
            }
            return $result;
        } catch (\Exception $ex) {
            $this->helperData->writeLog($ex->getMessage());
        }
        return $result;
    }
}
