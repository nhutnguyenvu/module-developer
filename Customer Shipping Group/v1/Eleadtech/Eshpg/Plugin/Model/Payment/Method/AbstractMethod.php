<?php
namespace Eleadtech\Eshpg\Plugin\Model\Payment\Method;

class AbstractMethod
{
    private $helperData;
    
    private $eshpg;
    
    public function __construct(
        \Eleadtech\Eshpg\Helper\Data $_helperData,
        \Eleadtech\Eshpg\Model\EshpgFactory $eshpg
    ) {

        $this->eshpg = $eshpg;
        $this->helperData = $_helperData;
    }

    public function aroundIsAvailable(\Magento\Payment\Model\Method\AbstractMethod $paymentMethod, $process, \Magento\Quote\Api\Data\CartInterface $quote = null)
    {
        try {
            if ($this->helperData->isEnabled()) {
                if ($quote) {
                    $currentGroupId = $quote->getCustomerGroupId();
                    $store = $this->helperData->getStoreManager()->getStore($quote->getStoreId());
                    $websiteId = $store->getWebsiteId();
                    $eshpgObj = $this->eshpg->create()->loadByGroupId($currentGroupId, $websiteId);
                    if (!$eshpgObj) {
                        $result = $process($quote);
                        return $result;
                    } else {
                        $paymentCodes = $eshpgObj->getPaymentCodes();
                        if (!empty($paymentCodes)) {
                            $paymentCodesArr = explode(",", $paymentCodes);
                            $code = $paymentMethod->getCode();
                            if (!in_array($code, $paymentCodesArr)) {
                                return false;
                            }
                        }
                    }
                }
            }
            $result = $process($quote);
        } catch (\Exception $ex) {
            $this->helperData->writeLog($ex->getMessage());
        }
        return $result;
    }
}
