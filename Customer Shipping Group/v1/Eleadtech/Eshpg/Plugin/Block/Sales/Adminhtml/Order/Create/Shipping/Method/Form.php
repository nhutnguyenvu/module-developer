<?php
namespace Eleadtech\Eshpg\Plugin\Block\Sales\Adminhtml\Order\Create\Shipping\Method;

use Magento\Store\Model\StoreManagerInterface;

class Form
{

    private $customerSession;
    private $eshpg;
    private $helperData;

    public function __construct(
        \Eleadtech\Eshpg\Helper\Data $_helperData,
        \Eleadtech\Eshpg\Model\EshpgFactory $eshpg
    ) {
        $this->eshpg = $eshpg;
        $this->helperData = $_helperData;
    }

    public function afterGetShippingRates(
        \Magento\Sales\Block\Adminhtml\Order\Create\Shipping\Method\Form $form,
        $result
    ) {

        if ($this->helperData->isEnabled()) {
            $quote = $form->getQuote();
           
            $customerGroupId = $quote->getCustomerGroupId();

            $store = $this->helperData->getStoreManager()->getStore($quote->getStoreId());
            $websiteId = $store->getWebsiteId();

            $eshpgObj = $this->eshpg->create()->loadByGroupId($customerGroupId, $websiteId);

            if (!$eshpgObj) {
                return $result;
            } else {
                $shippingCodes = $eshpgObj->getShippingCodes();

                $newResult = [];

                if (!empty($shippingCodes)) {
                    if (!empty($result)) {
                        $shippingCodesArr = explode(",", $shippingCodes);
                        foreach ($result as $codeCarrie => $method) {
                            if (in_array($codeCarrie, $shippingCodesArr)) {
                                $newResult[$codeCarrie] = $method;
                            }
                        }

                        return $newResult;
                    }
                }
            }
        }
        return $result;
    }
}
