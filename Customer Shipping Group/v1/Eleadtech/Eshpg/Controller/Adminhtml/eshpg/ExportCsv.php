<?php

namespace Eleadtech\Eshpg\Controller\Adminhtml\eshpg;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;

class ExportCsv extends \Magento\Backend\App\Action
{
    private $fileFactory;

    public function execute()
    {
        $this->_view->loadLayout(false);

        $fileName = 'eshpg.csv';

        $exportBlock = $this->_view->getLayout()->createBlock('Eleadtech\Eshpg\Block\Adminhtml\Eshpg\Grid');

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $this->fileFactory = $objectManager->create('Magento\Framework\App\Response\Http\FileFactory');

        return $this->fileFactory->create(
            $fileName,
            $exportBlock->getCsvFile(),
            DirectoryList::VAR_DIR
        );
    }
}
