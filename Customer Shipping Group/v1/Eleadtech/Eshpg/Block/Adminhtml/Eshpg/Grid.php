<?php
namespace Eleadtech\Eshpg\Block\Adminhtml\Eshpg;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{

    /**
     * @var \Magento\Framework\Module\Manager
     */
    private $moduleManager;

    /**
     * @var \Eleadtech\Eshpg\Model\eshpgFactory
     */
    private $eshpgFactory;

    /**
     * @var \Eleadtech\Eshpg\Model\Status
     */
    private $status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Eleadtech\Eshpg\Model\eshpgFactory $eshpgFactory
     * @param \Eleadtech\Eshpg\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Eleadtech\Eshpg\Model\EshpgFactory $EshpgFactory,
        \Eleadtech\Eshpg\Model\Status $status,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {
        $this->eshpgFactory = $EshpgFactory;
        $this->status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->eshpgFactory->create()->getCollection();
        $joinConditions = 'main_table.group_id = customer_group.customer_group_id';
        $collection->getSelect()->join(
            ['customer_group'],
            $joinConditions,
            ['customer_group_code' => 'customer_group.customer_group_code']
        );

        $this->setCollection($collection);

        parent::_prepareCollection();

        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );

        $this->addColumn(
            'customer_group_code',
            [
                'header' => __('Customer Group'),
                'index' => 'customer_group_code',
            ]
        );

        $this->addColumn(
            'shipping_codes',
            [
                'header' => __('Shippings'),
                'sortable' => true,
                'index' => 'shipping_codes',
                'renderer' => 'Eleadtech\Eshpg\Block\Adminhtml\Eshpg\Edit\Tab\Renderer\Shipping'
            ]
        );

        $this->addColumn(
            'payment_codes',
            [
                'header' => __('Payments'),
                'index' => 'payment_codes',
                'renderer' => 'Eleadtech\Eshpg\Block\Adminhtml\Eshpg\Edit\Tab\Renderer\Payments'
            ]
        );

        $this->addColumn(
            'website_ids',
            [
                'header' => __('Wesites'),
                'sortable' => true,
                'index' => 'website_ids',
                'filter' => false,
                'renderer' => 'Eleadtech\Eshpg\Block\Adminhtml\Eshpg\Edit\Tab\Renderer\Website'
            ]
        );

        $this->addColumn(
            'edit',
            [
            'header' => __('Edit'),
            'type' => 'action',
            'getter' => 'getId',
            'actions' => [
                [
                    'caption' => __('Edit'),
                    'url' => [
                        'base' => '*/*/edit'
                    ],
                    'field' => 'id'
                ]
            ],
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
            'header_css_class' => 'col-action',
            'column_css_class' => 'col-action'
            ]
        );

        $this->addExportType($this->getUrl('eshpg/*/exportCsv', ['_current' => true]), __('CSV'));
        $this->addExportType($this->getUrl('eshpg/*/exportExcel', ['_current' => true]), __('Excel XML'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('eshpg');

        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => __('Delete'),
                'url' => $this->getUrl('eshpg/*/massDelete'),
                'confirm' => __('Are you sure?')
            ]
        );

        $statuses = $this->status->getOptionArray();

        $this->getMassactionBlock()->addItem(
            'status',
            [
                'label' => __('Change status'),
                'url' => $this->getUrl('eshpg/*/massStatus', ['_current' => true]),
                'additional' =>
                [
                    'visibility' =>
                    [
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('Status'),
                        'values' => $statuses
                    ]
                ]
            ]
        );

        return $this;
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('eshpg/*/index', ['_current' => true]);
    }

    /**
     * @param \Eleadtech\Eshpg\Model\eshpg|\Magento\Framework\Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return false;
    }
}
