<?php
namespace Eleadtech\Eshpg\Block\Adminhtml\Eshpg\Edit\Tab\Renderer;

use Magento\Framework\DataObject;

class Payments extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    private $helperData;

    /**
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     */
    public function __construct(\Eleadtech\Eshpg\Helper\Data $helperData)
    {
        $this->helperData = $helperData;
    }

    /**
     * get category name
     * @param  DataObject $row
     * @return string
     */
    public function render(DataObject $row)
    {
        $paymentCodes = $row->getPaymentCodes();

        $paymentCodes = explode(",", $paymentCodes);

        return $this->helperData->getPaymentTitlesByPaymentCodes($paymentCodes);
    }
}
