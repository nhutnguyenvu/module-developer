<?php
namespace Eleadtech\Eshpg\Block\Adminhtml\Eshpg\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('eshpg_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Information'));
    }
}
