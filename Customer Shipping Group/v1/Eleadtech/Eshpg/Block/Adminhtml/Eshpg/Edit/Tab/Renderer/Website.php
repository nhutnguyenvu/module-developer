<?php
namespace Eleadtech\Eshpg\Block\Adminhtml\Eshpg\Edit\Tab\Renderer;

use Magento\Framework\DataObject;

class Website extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    private $systemStore;

    /**
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     */
    public function __construct(
        \Magento\Store\Model\System\Store $systemStore
    ) {
        $this->systemStore = $systemStore;
    }

    /**
     * get category name
     * @param  DataObject $row
     * @return string
     */
    public function render(DataObject $row)
    {
        $websiteIds = $row->getWebsiteIds();
        $websiteIds = explode(",", $websiteIds);
        $websiteStr = "";
        $websites = $this->systemStore->getWebsiteValuesForForm(false, false);
        if (!empty($websites)) {
            foreach ($websites as $values) {
                if (in_array($values['value'], $websiteIds)) {
                    $websiteStr .= ", " . $values['label'];
                }
            }
            $websiteStr = trim($websiteStr, ", ");
        }

        return $websiteStr;
    }
}
