<?php
namespace Eleadtech\Eshpg\Block\Adminhtml\Eshpg\Edit\Tab;

/**
 * Eshpg edit form main tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{

    /**
     * @var \Magento\Store\Model\System\Store
     */
    private $systemStore;

    /**
     * @var \Eleadtech\Eshpg\Model\Status
     */
    private $status;
    private $helperData;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Eleadtech\Eshpg\Model\Status $status,
        \Eleadtech\Eshpg\Helper\Data $helperData,
        array $data = []
    ) {
        $this->systemStore = $systemStore;
        $this->status      = $status;
        $this->helperData  = $helperData;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('eshpg');

        $isElementDisabled = false;

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Item Information')]);

        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', ['name' => 'id']);
        }

        $fieldset->addField(
            'group_id',
            'select',
            [
                'name'     => 'group_id',
                'label'    => __('Group'),
                'title'    => __('Group'),
                'values'   => $this->helperData->getGroupCustomer(),
                'disabled' => $isElementDisabled
            ]
        );

        $this->helperData->getShippingMethods();

        $fieldset->addField(
            'shipping_codes',
            'multiselect',
            [
                'name'     => 'shipping_codes[]',
                'label'    => __('Shippings'),
                'title'    => __('Shippings'),
                'required' => false,
                'values'   => $this->helperData->getShippingMethods(),
                'disabled' => $isElementDisabled
            ]
        );

        $fieldset->addField(
            'payment_codes',
            'multiselect',
            [
                'name'     => 'payment_codes[]',
                'label'    => __('Payments'),
                'title'    => __('Payments'),
                'required' => false,
                'values'   => $this->helperData->getPaymentMethods(),
                'disabled' => $isElementDisabled
            ]
        );

        $fieldset->addField(
            'website_ids',
            'multiselect',
            [
                'name'     => 'website_ids[]',
                'label'    => __('Websites'),
                'title'    => __('Websites'),
                'required' => true,
                'values'   => $this->systemStore->getWebsiteValuesForForm(false, false),
            ]
        );

        if (!$model->getId()) {
            $model->setData('is_active', $isElementDisabled ? '0' : '1');
        }

        $data = $model->getData();

        if (!isset($data['shipping_codes'])) {
            $data['shipping_codes'] = "0";
        }

        if (!isset($data['payment_codes'])) {
            $data['payment_codes'] = "0";
        }

        $form->setValues($data);

        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Item Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Item Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    public function getTargetOptionArray()
    {
        return [
            '_self' => "Self",
            '_blank' => "New Page"
        ];
    }
}
