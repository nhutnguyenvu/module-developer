<?php
namespace Eleadtech\Eshpg\Block\Adminhtml\Eshpg\Edit\Tab\Renderer;

use Magento\Framework\DataObject;

class Shipping extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    private $helperData;

    /**
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     */
    public function __construct(\Eleadtech\Eshpg\Helper\Data $helperData)
    {
        $this->helperData = $helperData;
    }

    /**
     * get category name
     * @param  DataObject $row
     * @return string
     */
    public function render(DataObject $row)
    {
        $shippingCodes = $row->getShippingCodes();

        $shippingCodes = explode(",", $shippingCodes);

        return $this->helperData->getShippingTitlesByShippingCodes($shippingCodes);
    }
}
