<?php

namespace Eleadtech\Eshpg\Helper;

use Eleadtech\Eshpg\Logger\Logger;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Framework\App\Config\MutableScopeConfigInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{

    
    private $urlBuilder = null;
    private $storeManagerInterface;
    private $logger;
    private $resourceConfig;
    private $shippingConfig;
    private $paymentConfig;
    private $date;
    private $groupCustomer;
    private $paymentMethodFactory;
    private $state;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        UrlInterface $urlBuilder,
        StoreManagerInterface $storeManagerInterface,
        Config $resourceConfig,
        Logger $logger,
        DateTime $date,
        \Magento\Shipping\Model\Config $shippingConfig,
        \Magento\Payment\Model\Config $paymentConfig,
        \Magento\Customer\Model\Customer\Source\Group $groupCustomer,
        \Magento\Payment\Model\Method\Factory $paymentMethodFactory,
        \Magento\Framework\App\State $state
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->scopeConfig = $scopeConfig;
        $this->storeManagerInterface = $storeManagerInterface;
        $this->resourceConfig = $resourceConfig;
        $this->logger = $logger;
        $this->date = $date;
        $this->shippingConfig = $shippingConfig;
        $this->paymentConfig = $paymentConfig;
        $this->groupCustomer = $groupCustomer;
        $this->paymentMethodFactory = $paymentMethodFactory;
        $this->state = $state;
    }

    public function getStoreManager()
    {
        return $this->storeManagerInterface;
    }

    public function isAdminPage()
    {
        return 'adminhtml' === $this->state->getAreaCode();
    }

    public function isEnabled()
    {

        $value = $this->scopeConfig->getValue('eshpg/general/enable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        return $value;
    }

    public function getAllMagentoPaymentMethods()
    {
        $methods = [];
        foreach ($this->scopeConfig->getValue('payment', ScopeInterface::SCOPE_STORE, null) as $code => $data) {
            if (isset($data['model'])) {
                $dataModel = $data['model'];
                if ($dataModel) {
                    /** @var MethodInterface $methodModel Actually it's wrong interface */
                    $methodModel = $this->paymentMethodFactory->create($data['model']);
                    $methodModel->setStore(null);
                    $methods[$code] = $methodModel;
                }
            }
        }

        return $methods;
    }

    public function exceptionPayments()
    {
        return false;
    }

    public function writeLog($message)
    {
        $this->logger->info($message);
    }

    public function getShippingMethods()
    {

        $carriers = $this->shippingConfig->getAllCarriers();
        $shippingMethods = [];
        $shippingMethodsActive = [];
        $shippingMethodsInactive = [];
        if (!empty($carriers)) {
            $i = 0;
            $shippingMethods[$i]["label"] = "All Shippments";
            $shippingMethods[$i]["value"] = "0";
            $i++;
            $countActive = 0;
            $countInActive = 0;
            foreach ($carriers as $code => $model) {
                $active = $model->getConfigData("active");
                if ($active) {
                    $shippingMethodsActive[$countActive]["label"] = $model->getConfigData("title") . (" (Enabled)");
                    $shippingMethodsActive[$countActive]["value"] = $code;
                    $countActive++;
                } else {
                    $shippingMethodsInactive[$countInActive]["label"] = $model->getConfigData("title").(" (Disabled)");
                    $shippingMethodsInactive[$countInActive]["value"] = $code;
                    $countInActive++;
                }
            }
            
            $shippingMethods = array_merge($shippingMethods, array_merge($shippingMethodsActive, $shippingMethodsInactive));
        }

        return $shippingMethods;
    }

    public function getPaymentMethods()
    {

        $payments = $this->getAllMagentoPaymentMethods();

        $paymentMethods = [];
        $paymentMethodsActive = [];
        $paymentMethodsInactive = [];
        if (!empty($payments)) {
            $i = 0;
            $paymentMethods[$i]["label"] = "All Payments";
            $paymentMethods[$i]["value"] = "0";
            $i++;
            $countActive = 0;
            $countInActive = 0;
            foreach ($payments as $code => $model) {
                $active = $model->getConfigData("active");
                if ($active) {
                    $paymentMethodsActive[$countActive]["label"] = $model->getConfigData("title") . " (" . $code . ")" . " - Enabled";
                    $paymentMethodsActive[$countActive]["value"] = $code;
                    $countActive++;
                } else {
                    $paymentMethodsInactive[$countInActive]["label"] = $model->getConfigData("title")." (" . $code . ")"." - Disabled";
                    $paymentMethodsInactive[$countInActive]["value"] = $code;
                    $countInActive++;
                }
            }

            $paymentMethods = array_merge($paymentMethods, array_merge($paymentMethodsActive, $paymentMethodsInactive));
        }

        return $paymentMethods;
    }

    public function getGroupCustomer()
    {

        $groups = $this->groupCustomer->toOptionArray();

        $allSingleGroups = [];
        foreach ($groups as $key => $group) {
            if ($group['value'] < 30000) {
                $allSingleGroups[$key] = $group;
            }
        }

        return $allSingleGroups;
    }

    public function getShippingTitlesByShippingCodes($shippingCodes)
    {

        $shippingMethods = $this->getShippingMethods();
        $shippingTitle = "";
        if (!empty($shippingMethods)) {
            foreach ($shippingMethods as $index => $method) {
                if (in_array($method['value'], $shippingCodes)) {
                    $shippingTitle .= $method['label'] . ', ';
                }
            }
        }
        return trim(trim($shippingTitle), ",");
    }

    public function getPaymentTitlesByPaymentCodes($paymentCodes)
    {

        $paymentMethods = $this->getPaymentMethods();

        $paymentTitle = "";
        if (!empty($paymentMethods)) {
            foreach ($paymentMethods as $index => $method) {
                if (in_array($method['value'], $paymentCodes)) {
                    $paymentTitle .= $method['label'] . ', ';
                }
            }
        }
        return trim(trim($paymentTitle), ",");
    }
}
