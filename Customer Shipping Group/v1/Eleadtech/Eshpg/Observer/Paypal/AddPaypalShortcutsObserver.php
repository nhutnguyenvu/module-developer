<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Eleadtech\Eshpg\Observer\Paypal;

use Magento\Paypal\Helper\Shortcut\Factory;
use Magento\Framework\Event\ObserverInterface;
use Magento\Paypal\Model\Config as PaypalConfig;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Store\Model\StoreManagerInterface;

/**
 * PayPal module observer
 */
class AddPaypalShortcutsObserver implements ObserverInterface
{

    /**
     * @var Factory
     */
    private $shortcutFactory;

    /**
     * @var PaypalConfig
     */
    private $paypalConfig;
    private $helperData;
    private $customerSession;
    private $eshpg;

    /**
     * Constructor
     *
     * @param Factory $shortcutFactory
     * @param PaypalConfig $paypalConfig
     */
    public function __construct(
        Factory $shortcutFactory,
        PaypalConfig $paypalConfig,
        \Eleadtech\Eshpg\Helper\Data $_helperData,
        \Magento\Customer\Model\Session $customerSession,
        \Eleadtech\Eshpg\Model\EshpgFactory $eshpg
    ) {
        $this->shortcutFactory = $shortcutFactory;
        
        $this->paypalConfig = $paypalConfig;
        
        $this->helperData = $_helperData;
        
        $this->customerSession = $customerSession;
        
        $this->eshpg = $eshpg;
    }

    /**
     * Add PayPal shortcut buttons
     *
     * @param EventObserver $observer
     * @return void
     */
    public function execute(EventObserver $observer)
    {
        /** @var \Magento\Catalog\Block\ShortcutButtons $shortcutButtons */
        $shortcutButtons = $observer->getEvent()->getContainer();
        $blocks =
        [
            'Magento\Paypal\Block\Express\InContext\Minicart\Button' => PaypalConfig::METHOD_WPS_EXPRESS,
            'Magento\Paypal\Block\Express\Shortcut' => PaypalConfig::METHOD_WPP_EXPRESS,
            'Magento\Paypal\Block\Bml\Shortcut' => PaypalConfig::METHOD_WPP_EXPRESS,
            'Magento\Paypal\Block\WpsExpress\Shortcut' => PaypalConfig::METHOD_WPS_EXPRESS,
            'Magento\Paypal\Block\WpsBml\Shortcut' => PaypalConfig::METHOD_WPS_EXPRESS,
            'Magento\Paypal\Block\PayflowExpress\Shortcut' => PaypalConfig::METHOD_WPP_PE_EXPRESS,
            'Magento\Paypal\Block\Payflow\Bml\Shortcut' => PaypalConfig::METHOD_WPP_PE_EXPRESS
        ];
        foreach ($blocks as $blockInstanceName => $paymentMethodCode) {
            if (!$this->paypalConfig->isMethodAvailable($paymentMethodCode)) {
                continue;
            }

            if ($this->helperData->isEnabled()) {
                $isAvailable = $this->isAvailableShortCutButton($blockInstanceName, $paymentMethodCode);
                if (!$isAvailable) {
                    continue;
                }
            }

            $params = [
                'shortcutValidator' => $this->shortcutFactory->create($observer->getEvent()->getCheckoutSession()),
            ];

            if (!in_array('Bml', explode('\\', $blockInstanceName))) {
                $params['checkoutSession'] = $observer->getEvent()->getCheckoutSession();
            }

            // we believe it's \Magento\Framework\View\Element\Template
            $shortcut = $shortcutButtons->getLayout()->createBlock(
                $blockInstanceName,
                '',
                $params
            );
            $shortcut->setIsInCatalogProduct(
                $observer->getEvent()->getIsCatalogProduct()
            )->setShowOrPosition(
                $observer->getEvent()->getOrPosition()
            );
            $shortcutButtons->addShortcut($shortcut);
        }
    }

    protected function isAvailableShortCutButton($blockInstanceName, $paymentMethodCode)
    {

        if ($this->customerSession->isLoggedIn()) {
            $currentGroupId = $this->customerSession->getCustomer()->getGroupId();
        } else {
            $currentGroupId = 0;
        }

        $store = $this->helperData->getStoreManager()->getStore();
        $websiteId = $store->getWebsiteId();
        $eshpgObj = $this->eshpg->create()->loadByGroupId($currentGroupId, $websiteId);

        if ($eshpgObj) {
            $paymentCodes = $eshpgObj->getPaymentCodes();

            if (!empty($paymentCodes)) {
                if (in_array('Bml', explode('\\', $blockInstanceName))) {
                    $paymentMethodCode = $paymentMethodCode . "_bml";
                }
                if (in_array('Express', explode('\\', $blockInstanceName))) {
                    $paymentMethodCode = "paypal_express";
                }

                $paymentCodesArr = explode(",", $paymentCodes);
                if (!in_array($paymentMethodCode, $paymentCodesArr)) {
                    return false;
                }
            }
        }
        return true;
    }
}
