<?php

namespace Eleadtech\Productextrarate\Plugin\Model\Shipping;

class Shipping {

    
    protected $_helperData;
    protected $_cart;
    protected $_sessionQuote;

    public function __construct(
        \Eleadtech\Productextrarate\Helper\Data $_helperData,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Backend\Model\Session\Quote $sessionQuote
       
        ) 
    {
        
        $this->_helperData = $_helperData;
        $this->_cart = $cart;
        $this->_sessionQuote = $sessionQuote;
        
    }
    
    public function afterCollectRates(\Magento\Shipping\Model\Shipping $shipping, $returnResult) {
        
        $enableModule = $this->_helperData->isEnabled();
        $quote = null;
        if($enableModule){
            $result = $returnResult->getResult();
            if($result){
                
                //$frontend = $this->_helperData->getStoreManager()->getStore()->isFrontUrlSecure();
                $adminPage = $this->_helperData->isAdminPage();
                
                if($adminPage){
                    
                    $quote =  $this->_sessionQuote->getQuote();
                }
                else{
                    $quote =  $this->_cart->getQuote();
                }
                
                $shippingPrices = $this->_helperData->getExtraPriceForByShippingCodesAndSkus($quote);
                
                if(count($shippingPrices) > 0){
                    $allRates = $result->getAllRates();
                    
                    if(count($allRates) > 0){
                        foreach ($allRates as $rate){
                            if($rate instanceof \Magento\Quote\Model\Quote\Address\RateResult\AbstractResult ){
                                $method = $rate->getCarrier();
                                if(isset($shippingPrices[$method])){
                                    $rate->setPrice($rate->getPrice() + $shippingPrices[$method]);
                                } 
                            }
                        }  
                    }   
                }

                
                if($quote){
                    $shippingAddress = $quote->getShippingAddress();
                    if($shippingAddress){
                        $method = $shippingAddress->getShippingMethod();
                        if(!empty($method)){
                            $methodArr = explode("_",$method);
                            $mcode = @$methodArr[0];
                            $quote->setBaseTotalExtraPrice(0);
                            $quote->setTotalExtraPrice(0);

                            if(isset($shippingPrices[$mcode])){
                                $quote->setBaseTotalExtraPrice($this->_helperData->convertPrice($shippingPrices[$mcode]));
                                $quote->setTotalExtraPrice($shippingPrices[$mcode]);
                            }
                            $quote->save();
                        }
                    }
                }

            }
        }
       
        return $returnResult;
        
    }
   
}