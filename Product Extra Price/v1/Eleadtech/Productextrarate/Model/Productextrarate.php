<?php
namespace Eleadtech\Productextrarate\Model;

class Productextrarate extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected $_storeManager;
    
    const FIXED_ON_ALL_ITEM = 1;
    const PRICE_FOR_EACH_ITEM = 2;
    const ALL_WEBSITES = "0";
    
    
    protected function _construct()
    {
        
        $this->_init('Eleadtech\Productextrarate\Model\ResourceModel\Productextrarate');
    }
    /*
    public function getShippingItemExtraPrice($sku,$qty){
        
        $qty = floatval($qty);
        
        $itemFirst = $this->getCollection()
                ->addFieldToSelect("*")
                ->addFieldToFilter("sku",$sku)
                ->addFieldToFilter("min_qty", array("lteq"=>$qty))
                ->addFieldToFilter("max_qty", array("gteq"=>$qty))
                ->getFirstItem();
        
        if($itemFirst->getId()){
            $type = $itemFirst->getExtraType();
            
            if($type==self::FIXED_ON_ALL_ITEM){
                return $itemFirst->getExtraPrice();
            }
            else if($type == self::PRICE_FOR_EACH_ITEM){
                
                return floatval($itemFirst->getExtraPrice()) * $qty;
            }
        }
        return 0;        
    }
    */
    
    public function getExtraPriceForByShippingCodes(array $shippingCodes, $skus, $qtys,$websiteId){ 
        
        return $this->getResource()->getExtraPriceForByShippingCodes($shippingCodes, $skus, $qtys,$websiteId);
        
    }
}
?>