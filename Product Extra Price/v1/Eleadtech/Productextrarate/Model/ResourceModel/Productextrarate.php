<?php

namespace Eleadtech\Productextrarate\Model\ResourceModel;

class Productextrarate extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct() {
        $this->_init('eproduct_extra_rate', 'id');
    }
    
    public function getExtraPriceForByShippingCodes($shippingCodes, $skus, $qtys, $websiteId) {
       
        $mSkus = []; 
        
        if(count($skus) > 0){
            for($i=0; $i < count($skus); $i++){
                $mSkus[$i] =  "'". $skus[$i] . "'";
            }
        }
        
        $skusString = implode(",", $mSkus);
        
        $table = $this->getTable("eproduct_extra_rate");
        $mysqlString = "Select * FROM $table ";
        /*$mysqlString .= "Where (min_qty <= $qty OR min_qty = ~) AND  (max_qty >= $qty OR max_qty = ~) ";*/
        $mysqlString .= " WHERE FIND_IN_SET('$websiteId',website_ids) ";
        $mysqlString .= "AND SKU IN ($skusString)";
        
        if(count($shippingCodes) > 0){
            for ($i = 0; $i < count($shippingCodes); $i++) {
                if($i==0){
                    $mysqlString .= " AND ((FIND_IN_SET('$shippingCodes[$i]',shipping_codes) OR shipping_codes = 0) ";
                }
                else{
                    $mysqlString .= " OR (FIND_IN_SET('$shippingCodes[$i]',shipping_codes) OR shipping_codes = 0)";
                }
                if($i == count($shippingCodes) - 1){
                    $mysqlString .= ")";
                }
            }
           
            $rows = $this->getConnection()->fetchAll($mysqlString);
            $priceShippings = array();
            $existingCalculating = array();
            
            $rows = $this->_filterByQtys($rows, $skus, $qtys);
      
            if (!empty($rows)) {
                if (count($shippingCodes) > 0) {
                    foreach ($shippingCodes as $key => $shippingCode) {
                        $priceShippings[$shippingCode] = 0;
                        for ($i = 0; $i < count($rows); $i++) {

                            $sku = $rows[$i]['sku'];
                            
                            $dbShippingCodes = explode(",",$rows[$i]['shipping_codes']);
                            
                            $shippingSku = $shippingCode . "_" . $sku;

                            if ((in_array($shippingCode, $dbShippingCodes) || $dbShippingCodes[0] === 
                                    \Eleadtech\Productextrarate\Model\Productextrarate::ALL_WEBSITES) && !in_array($shippingSku, $existingCalculating) ) {
                                
                                $type = $rows[$i]['extra_type'];
                                if($type == \Eleadtech\Productextrarate\Model\Productextrarate::FIXED_ON_ALL_ITEM){
                                    $priceShippings[$shippingCode] += $rows[$i]['extra_price'];
                                }
                                else if($type == \Eleadtech\Productextrarate\Model\Productextrarate::PRICE_FOR_EACH_ITEM){
                                    $priceShippings[$shippingCode] += $rows[$i]['extra_price'] * $qtys[array_search($sku, $skus)];
                                }
                                
                                $existingCalculating[] = $shippingCode . "_" . $sku;
                            }
                        }
                    }
                }
                
                return $priceShippings;
            } 
        }
        
        return false;
    }
    
    protected function  _filterByQtys($rows, $skus, $qtys){
        
        $newRows = array();
        
        if(count($rows) > 0){
            foreach ($rows as $row ){
                $positionSku = array_search($row['sku'],$skus);
                if(
                        $row['min_qty'] <= $qtys[$positionSku] && $row['max_qty'] >= $qtys[$positionSku]
                        || $row['min_qty'] <= $qtys[$positionSku] && $row['max_qty'] == "-1"
                        || $row['min_qty'] == "-1" && $row['max_qty'] >= $qtys[$positionSku]
                        )
                {
                    $newRows[] = $row;
                }
            }
        }
        return $newRows;
    }
}

?>