<?php

namespace Eleadtech\Productextrarate\Model\ResourceModel\Productextrarate;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Eleadtech\Productextrarate\Model\Productextrarate', 'Eleadtech\Productextrarate\Model\ResourceModel\Productextrarate');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
?>