<?php

namespace Eleadtech\Productextrarate\Helper;

use Eleadtech\Productextrarate\Logger\Logger;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Framework\App\Config\MutableScopeConfigInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper {

    protected $_scopeConfig = null;
    protected $_urlBuilder = null;
    protected $_storeManagerInterface;
    protected $_logger;
    protected $_resourceConfig;
    protected $_shippingConfig;
    protected $_date;
    protected $_groupCustomer;
    protected $_productExtraModel;
    protected $_priceCurrency;
    protected $_modelProduct;
    protected $_state;

    public function __construct(
        ScopeConfigInterface $scopeConfig, 
        UrlInterface $urlBuilder, StoreManagerInterface $storeManagerInterface, 
        Config $resourceConfig, Logger $logger, DateTime $date,
        \Eleadtech\Productextrarate\Model\ProductextrarateFactory $productExtraModel, 
        \Magento\Framework\Pricing\Helper\Data $priceCurrency,
        \Magento\Shipping\Model\Config $shippingConfig,
        \Magento\Catalog\Model\ProductFactory $_modelProduct,
        \Magento\Framework\App\State $state
        
        
            
    ) {
        $this->_urlBuilder = $urlBuilder;
        $this->_scopeConfig = $scopeConfig;
        $this->_storeManagerInterface = $storeManagerInterface;
        $this->_resourceConfig = $resourceConfig;
        $this->_logger = $logger;
        $this->_date = $date;
        $this->_productExtraModel = $productExtraModel;
        $this->_priceCurrency = $priceCurrency;
        $this->_shippingConfig = $shippingConfig;
        $this->_modelProduct = $_modelProduct;
        $this->_state = $state;
        
    }

    /**
     * Get formatted by price and currency
     *
     * @param   $price
     * @param   $currency
     * @return  array || float
     */
    
    
    public function isEnabled() {
        $value = $this->_scopeConfig->getValue('productextrarate/general/enable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $value;
    }

    public function getItemMessage() {
        $value = $this->_scopeConfig->getValue('productextrarate/general/item_message', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $value;
    }

    public function getShippingMessage() {
        $value = $this->_scopeConfig->getValue('productextrarate/general/shipping_message', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $value;
    }

    public static function getExtraType() {
        return [\Eleadtech\Productextrarate\Model\Productextrarate::FIXED_ON_ALL_ITEM => __('Fixed on all items'),
            \Eleadtech\Productextrarate\Model\Productextrarate::PRICE_FOR_EACH_ITEM => __('Price for each item')];
    }
    /*
    public function createItemMessage($product, $qty) {
       
        $price = $this->getExtraPrice($product->getSku(), $qty);
        
        $message = $this->getItemMessage();
        $messageArr = explode("%s", $message);

        if (count($messageArr) >= 1 && !empty($price)) {
            $price = $this->_priceCurrency->currency($price,true, false);
            return array("label" => @$messageArr[0], "value" => $price . " " . @$messageArr[1]);
        }
        return false;
    }
    */
    public function getExtraPrice($sku, $qty) {
        return $this->_productExtraModel->create()->getShippingItemExtraPrice($sku, $qty);
    }

    public function writeLog($message) {
        $this->_logger->info($message);
    }
    
    public function convertPrice($price){
        return $price;
    }
    
    public function calculateTotalExtraPrice($quote,$saveQuote=true){
        if($quote){
            $items = $quote->getAllVisibleItems();
            $totalExtraPrice = 0;
            $totalBaseExtraPrice = 0;
            if(count($items) > 0){
                foreach ($items as $item){
                    $totalExtraPrice += $item->getExtraPrice();
                    $totalBaseExtraPrice += $item->getBaseExtraPrice();
                }
                if($totalExtraPrice > 0){
                    $quote->setTotalExtraPrice($totalExtraPrice);
                    $quote->setBaseTotalExtraPrice($totalBaseExtraPrice);
                    if($saveQuote){      
                        $quote->save();
                    }
                } 
            }
            
        }
        return false;
    }
    
    public function getShippingMethods(){
        
        $carriers = $this->_shippingConfig->getAllCarriers();
        $shippingMethods = array();
        $shippingMethodsActive = array();
        $shippingMethodsInactive = array();
        if(count($carriers) > 0){
            $i =  0;
            $shippingMethods[$i]["label"] = "All Shippments";
            $shippingMethods[$i]["value"] = "0";
            $i++;
            $countActive = 0;
            $countInActive = 0;
            foreach ($carriers as $code => $model){
                
                $active = $model->getConfigData("active");
                if($active){
                    $shippingMethodsActive[$countActive]["label"] = $model->getConfigData("title") .(" (Enabled)")  ;
                    $shippingMethodsActive[$countActive]["value"] = $code;
                    $countActive++;
                }
                else{
                    $shippingMethodsInactive[$countInActive]["label"] = $model->getConfigData("title") .(" (Disabled)")  ;
                    $shippingMethodsInactive[$countInActive]["value"] = $code;
                    $countInActive++;
                }
                //echo $model->getConfigData("") 
            }
            
            $shippingMethods = array_merge($shippingMethods,array_merge($shippingMethodsActive,$shippingMethodsInactive));
            
        }
    
        return $shippingMethods;
            
    }
    
    public function getAllActiveShippingCodes(){
        
        $carriers = $this->_shippingConfig->getActiveCarriers();
        $shippingCodes = array();
        if(count($carriers)){
            foreach ($carriers as $code => $model){
                $shippingCodes[] = $code;
            }
        }
        return $shippingCodes;
    }
    
    public function getShippingTitlesByShippingCodes($shippingCodes){
        
        $shippingMethods = $this->getShippingMethods();
        $shippingTitle = "";
        if(count($shippingMethods) > 0){
            foreach ($shippingMethods as $index => $method){
                if(in_array($method['value'],$shippingCodes)){
                    $shippingTitle  .= $method['label'].  ', ';
                }
            }
        }
        return trim(trim($shippingTitle),",");
        
    }
    
    public function isAdminPage(){
       return 'adminhtml' === $this->_state->getAreaCode();
    }
    
    public function getExtraPriceForByShippingCodesAndSkus($quote=false){
        try{
            $skus = array();
            $qtys = array();
            if($quote){
                
                $items = $quote->getAllVisibleItems();
               
                if(count($items) > 0){
                    foreach ($items as $item){

                        if($item->getParentItemId()){
                            continue;
                        }
                        $productId = $item->getProductId();

                        $product = $this->_modelProduct->create()->load($productId);
                        $sku = $product->getSku();
                        
                        if(!in_array($sku,$skus)){
                            $skus[] = $product->getSku();
                            $qtys[] = $item->getQty();
                        }
                        else{
                            $position = array_search($sku,$skus);
                            $qtys[$position] += $item->getQty();
                        }
                        unset($product);
                    }
                    
                    $shippingCodes = $this->getAllActiveShippingCodes();
                    
                    $store = $this->_storeManagerInterface->getStore($quote->getStoreId());
                    $websiteId = $store->getWebsiteId();
                    
                    return $this->_productExtraModel->create()->getExtraPriceForByShippingCodes($shippingCodes,$skus,$qtys,$websiteId);
                }

               
            }
        } catch (Exception $ex) {
            $this->writeLog($ex->getMessage());
        }
        return false;
        
    }
    public function getStoreManager(){
        return $this->_storeManagerInterface;
    }
}
