<?php
namespace Eleadtech\Productextrarate\Controller\Adminhtml\productextrarate;

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;


class Save extends \Magento\Backend\App\Action
{

    /**
     * @param Action\Context $context
     */
    public function __construct(Action\Context $context)
    {
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        
        
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $model = $this->_objectManager->create('Eleadtech\Productextrarate\Model\Productextrarate');

            $id = $this->getRequest()->getParam('id');
            if ($id) {
                $model->load($id);
                $model->setCreatedAt(date('Y-m-d H:i:s'));
            }
			
            $newData = $this->proccessData($data);
            
            $model->setData($newData);
            
            

            try {
                $model->save();
                $this->messageManager->addSuccess(__('The Productextrarate has been saved.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the Productextrarate.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
    public function proccessData($data){
        
        $newData = $data;
        foreach ($data as $key => $value){
            
            if($key=="shipping_codes"){
                if(is_array($value) && count($value) > 0){
                    if(in_array("0",$value)){
                        $newData[$key] = "0";
                    }   
                    else{
                        $newData[$key] = implode(",",$value);
                    }
                }
                else{
                    $newData[$key] = "0";
                }
            }
            
            if($key=="website_ids"){
                if(is_array($value) && count($value) > 0){
                    $newData[$key] = implode(",",$value);  
                }
            }
        }
        
        return $newData;
    }
}