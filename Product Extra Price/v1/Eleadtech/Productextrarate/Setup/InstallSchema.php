<?php 
namespace Eleadtech\Productextrarate\Setup;
 
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (!$setup->tableExists('eproduct_extra_rate')) {
            
            $table = $setup->getConnection()->newTable(
                $setup->getTable('eproduct_extra_rate')
            )
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Id'
            )
            ->addColumn(
                'sku',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Sku'
            )
            ->addColumn(
                'extra_price',
                \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT,
                255,
                [],
                'Extra Price'
            )
            ->addColumn(
                'extra_type',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                2,
                [],
                'Extra Price'
            )
            ->addColumn(
                'min_qty',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                11,
                [],
                'Min Qty'
            )
           
            ->addColumn(
                'max_qty',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                11,
                [],
                'Max Qty'
            );
            
          
            $connection = $setup->getConnection();
            $connection->createTable($table);
            /**********************************************/
            
            $extraColumnsData [$setup->getTable('quote_item')] = [
                'base_extra_price' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '12,4',
                    'nullable' => false,
                    'comment' => 'Base Extra Price',
                ],
                'extra_price' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '12,4',
                    'nullable' => false,
                    'comment' => 'Extra Price',
                ]
            ];
            
            
            $extraColumnsData [$setup->getTable('quote')] = [
                'base_total_extra_price' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '12,4',
                    'nullable' => false,
                    'comment' => 'Base Extra Price',
                ],
                'total_extra_price' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '12,4',
                    'nullable' => false,
                    'comment' => 'Total Extra Price',
                ]
            ];
            
            $extraColumnsData [$setup->getTable('sales_order_item')] = [
                'base_extra_price' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '12,4',
                    'nullable' => false,
                    'comment' => 'Base Extra Price',
                ],
                'extra_price' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '12,4',
                    'nullable' => false,
                    'comment' => 'Extra Price',
                ]
            ];
            
            
            $extraColumnsData [$setup->getTable('sales_order')] = [
                'base_total_extra_price' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '12,4',
                    'nullable' => false,
                    'comment' => 'Base Total Extra Price',
                ],
                'total_extra_price' => [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '12,4',
                    'nullable' => false,
                    'comment' => 'Total Extra Price',
                ]
            ];

            foreach ($extraColumnsData as $table => $columns) {
                foreach ($columns as $name => $definition) {
                    $connection->addColumn($table, $name, $definition);
                } 
            }   
        }
        $setup->endSetup();
    }
}
?>