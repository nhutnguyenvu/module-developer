<?php
 
namespace Eleadtech\Productextrarate\Block\Adminhtml\Productextrarate\Edit\Tab\Renderer;
 
use Magento\Framework\DataObject;
 
class Website extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $_helperData;
    protected $_systemStore;
    /**
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     */
    public function __construct(
        \Eleadtech\Productextrarate\Helper\Data $helperData,
        \Magento\Store\Model\System\Store $systemStore
    ) {
        $this->_helperData = $helperData;
        $this->_systemStore = $systemStore;
    
    }
 
    /**
     * get category name
     * @param  DataObject $row
     * @return string
     */
    public function render(DataObject $row)
    {
        $websiteIds = $row->getWebsiteIds();
        $websiteIds = explode(",",$websiteIds);
        
        $websiteStr = "";
        $websites = $this->_systemStore->getWebsiteValuesForForm(false, false);
        if(count($websites)){
            foreach ($websites as $values){
                if(in_array($values['value'],$websiteIds)){
                    $websiteStr.= " " . $values['label'];
                }
            }
            $websiteStr = trim($websiteStr);
        }
        
        return $websiteStr;
    }
}