<?php

namespace Eleadtech\Productextrarate\Block\Adminhtml\Productextrarate\Edit\Tab;

/**
 * Productextrarate edit form main tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface {

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \Eleadtech\Productextrarate\Model\Status
     */
    protected $_status;
    protected $_helperData;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
    \Magento\Backend\Block\Template\Context $context, \Magento\Framework\Registry $registry, \Magento\Framework\Data\FormFactory $formFactory, \Magento\Store\Model\System\Store $systemStore, \Eleadtech\Productextrarate\Model\Status $status, \Eleadtech\Productextrarate\Helper\Data $helperData, array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_status = $status;
        $this->_helperData = $helperData;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm() {
        /* @var $model \Eleadtech\Productextrarate\Model\BlogPosts */
        $model = $this->_coreRegistry->registry('productextrarate');

        $isElementDisabled = false;

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Item Information')]);

        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', ['name' => 'id']);
        }


        $fieldset->addField(
            'sku', 'text',
                [
                'name' => 'sku',
                'label' => __('Product Sku'),
                'required' => true,
                'title' => __('Product Sku'),
                'width' => "50px"
                ]
        );


        $fieldset->addField(
            'min_qty', 'text', [
            'name' => 'min_qty',
            'label' => __('Min Qty'),
            'required' => true,
            'title' => __('Min Qty'),
            'disabled' => $isElementDisabled,
            'note' => 'use -1 if you do not mention on min value'
                ]
        );

        $fieldset->addField(
            'max_qty', 'text', [
                'name' => 'max_qty',
                'label' => __('Max Qty'),
                'required' => true,
                'title' => __('Max Qty'),
                'disabled' => $isElementDisabled,
                'note' => 'use -1 if you do not mention on max value'
            ]
        );

        $fieldset->addField(
            'extra_price', 'text', [
                'name' => 'extra_price',
                 'required' => true,
                'label' => __('Extra Shipping Price'),
                'title' => __('Extra Shipping Price'),
                'disabled' => $isElementDisabled
            ]
        );


        $fieldset->addField(
            'extra_type', 'select', [
                'name' => 'extra_type',
                'label' => __('Extra Type'),
                'title' => __('Extra Type'),
                'values' => $this->_helperData->getExtraType(),
                'disabled' => $isElementDisabled
            ]
        );

        $this->_helperData->getShippingMethods();

        $fieldset->addField(
                'shipping_codes', 'multiselect', [
            'name' => 'shipping_codes[]',
            'label' => __('Shipping Method'),
            'title' => __('Shipping Method'),
            'required' => true,
            'values' => $this->_helperData->getShippingMethods(),
            'disabled' => $isElementDisabled
                ]
        );

        $fieldset->addField(
                'website_ids', 'multiselect', [
            'name' => 'website_ids[]',
            'label' => __('Websites'),
            'title' => __('Websites'),
            'required' => true,
            'values' => $this->_systemStore->getWebsiteValuesForForm(false, false),
                ]
        );

        if (!$model->getId()) {
            $model->setData('is_active', $isElementDisabled ? '0' : '1');
        }

        $data = $model->getData();

        if (!isset($data['shipping_codes'])) {
            $data['shipping_codes'] = "0";
        }

        $form->setValues($data);

        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel() {
        return __('Item Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle() {
        return __('Item Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab() {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden() {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId) {
        return $this->_authorization->isAllowed($resourceId);
    }

    public function getTargetOptionArray() {
        return array(
            '_self' => "Self",
            '_blank' => "New Page",
        );
    }

}
