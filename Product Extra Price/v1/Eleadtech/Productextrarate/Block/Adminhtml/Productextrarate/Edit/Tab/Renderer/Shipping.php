<?php
 
namespace Eleadtech\Productextrarate\Block\Adminhtml\Productextrarate\Edit\Tab\Renderer;
 
use Magento\Framework\DataObject;
 
class Shipping extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $_helperData;
    /**
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     */
    public function __construct(
        \Eleadtech\Productextrarate\Helper\Data $helperData
    ) {
        $this->_helperData = $helperData;
    }
 
    /**
     * get category name
     * @param  DataObject $row
     * @return string
     */
    public function render(DataObject $row)
    {
        $shippingCodes = $row->getShippingCodes();
        
        $shippingCodes = explode(",",$shippingCodes);
        
        return $this->_helperData->getShippingTitlesByShippingCodes($shippingCodes);
    }
}