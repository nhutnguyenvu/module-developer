<?php

namespace Eleadtech\Productextrarate\Block\Adminhtml\Productextrarate;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended {

    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Eleadtech\Productextrarate\Model\productextrarateFactory
     */
    protected $_productextrarateFactory;

    /**
     * @var \Eleadtech\Productextrarate\Model\Status
     */
    protected $_status;
    
    protected $_helperData;
    
    protected $_systemStore;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Eleadtech\Productextrarate\Model\productextrarateFactory $productextrarateFactory
     * @param \Eleadtech\Productextrarate\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
    \Magento\Backend\Block\Template\Context $context, \Magento\Backend\Helper\Data $backendHelper, 
            \Eleadtech\Productextrarate\Model\ProductextrarateFactory $ProductextrarateFactory, 
            \Eleadtech\Productextrarate\Model\Status $status, 
            \Eleadtech\Productextrarate\Helper\Data $helperData,
            \Magento\Store\Model\System\Store $systemStore,
            \Magento\Framework\Module\Manager $moduleManager, array $data = []
    ) {
        $this->_productextrarateFactory = $ProductextrarateFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        $this->_helperData = $helperData;
        $this->_systemStore = $systemStore;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct() {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection() {
        
        
        $collection = $this->_productextrarateFactory->create()->getCollection();
        
        $this->setCollection($collection);

        parent::_prepareCollection();

        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns() {
        $this->addColumn(
                'id', [
            'header' => __('ID'),
            'type' => 'number',
            'index' => 'id',
            'header_css_class' => 'col-id',
            'column_css_class' => 'col-id'
                ]
        );



        $this->addColumn(
            
            'sku', 
            [
            'header' => __('Sku'),
            'index' => 'sku',
            'style' => 'width:30px',
                
            ]
        );

        $this->addColumn(
            'min_qty', [
            'header' => __('Min Qty'),
            'index' => 'min_qty',
                ]
        );

        $this->addColumn(
            'max_qty', [
            'header' => __('Max Qty'),
            'index' => 'max_qty',
                ]
        );

        $this->addColumn(
            'extra_price', [
            'header' => __('Extra Price'),
            'index' => 'extra_price',
                ]
        );

        $this->addColumn(
            'extra_type',
            [
                'header' => __('Extra Type'),
                'index' => 'extra_type',
                'type' => 'options',
                'options' => $this->_helperData->getExtraType()
                
            ]
            
        );
        
        $this->addColumn(
            'shipping_codes',
            [
                'header' => __('Shipping Methods'),
                'style' => 'width:300px',
                'sortable' => true,
                'index' => 'shipping_codes',
                'renderer'  => 'Eleadtech\Productextrarate\Block\Adminhtml\Productextrarate\Edit\Tab\Renderer\Shipping'
 
            ]
        );
        
        $this->addColumn(
            'website_ids',
            [
                'header' => __('Wesites'),
                'sortable' => true,
                'index' => 'website_ids',
                'filter'    => false,
                
                'renderer'  => 'Eleadtech\Productextrarate\Block\Adminhtml\Productextrarate\Edit\Tab\Renderer\Website'
                
 
            ]
        );
        
        $this->addColumn(
            'edit', [
            'header' => __('Edit'),
            'type' => 'action',
            'getter' => 'getId',
            'actions' => [
                [
                    'caption' => __('Edit'),
                    'url' => [
                        'base' => '*/*/edit'
                    ],
                    'field' => 'id'
                ]
            ],
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
            'header_css_class' => 'col-action',
            'column_css_class' => 'col-action'
                ]
        );



        $this->addExportType($this->getUrl('productextrarate/*/exportCsv', ['_current' => true]), __('CSV'));
        $this->addExportType($this->getUrl('productextrarate/*/exportExcel', ['_current' => true]), __('Excel XML'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction() {

        $this->setMassactionIdField('id');
        //$this->getMassactionBlock()->setTemplate('Eleadtech_Productextrarate::productextrarate/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('productextrarate');

        $this->getMassactionBlock()->addItem(
            'delete', [
            'label' => __('Delete'),
            'url' => $this->getUrl('productextrarate/*/massDelete'),
            'confirm' => __('Are you sure?')
                ]
        );

        $statuses = $this->_status->getOptionArray();

        $this->getMassactionBlock()->addItem(
                'status', [
            'label' => __('Change status'),
            'url' => $this->getUrl('productextrarate/*/massStatus', ['_current' => true]),
            'additional' => [
                'visibility' => [
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => __('Status'),
                    'values' => $statuses
                ]
            ]
                ]
        );


        return $this;
    }

    /**
     * @return string
     */
    public function getGridUrl() {
        return $this->getUrl('productextrarate/*/index', ['_current' => true]);
    }

    /**
     * @param \Eleadtech\Productextrarate\Model\productextrarate|\Magento\Framework\Object $row
     * @return string
     */
    
    public function getRowUrl($row) {
        return false;
        return $this->getUrl(
                        'productextrarate/*/edit', ['id' => $row->getId()]
        );
    }

}
